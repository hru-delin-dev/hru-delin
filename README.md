HRU delin next gen
==================
HRU delin used to work with grass6 only. This version brings compatibility with grass7 (and looses compatibility with grass6).
This repository contains hru-delin core (full Python3 and portable) and command line launch scripts (GNU/Linux only).

Differences with HRU delin v5
============================
Grass7 compatibility
Parallelized process in step2, step3 and step4
ZirkelKill ported to Python3, no more Java dependency
Launch scripts now accept an optional parameter to specify where to find the config file (with any name)
Advanced progression display
Automatic grass environment building, no more need to build a "case"
Flexibility in config file values path (accepts relative and absolute path)
Flexibility in user location when running launch scripts
New launch script to run all steps
No more system calls (to awk, grep, sed...) -> code is now full Python3 and portable to Windows
Use pipes between grass tools to avoid some hard disk access (avoid writing some intermediate files)


Windows
=======
It is technically possible to run it on Windows but there isn't any launch script. Launch scripts are written in BASH which is no installed on Windows by default.
It may be easy to write launch script in Windows batch or something else working on Windows but then it's not very easy to install hru-delin dependencies on Windows.
To use hru-delin on Windows, you can install the all-in-one hru-delin QGIS plugin. QGIS comes with all modules that are required to run hru-delin.

Install
=======
=======

Dependencies
===========
You'll need some Python3 modules to be able to run HRU delin.

TQDM: display progress
Pandas: read/write tabular files and perform some computations
dbf: write dbf versions of hru.par and reach.par

On Debian/Ubuntu/Mint/... you can install those with your package manager like that:
sudo apt install python3-pandas python3-dbf python3-tqdm
If you don't have administrator rights but still have pip3 installed:
pip3 install --user pandas dbf tqdm

HRU delin
=========
Installing hru-delin on your machine is pretty simple. There is no need to install it on the system.
Get the release archive and extract it somewhere. Then make sure the bin directory is in your PATH environment variable. Here is an example procedure:
# go to your home directory
cd
# extract the archive (adapt the path to the archive)
tar xvf Downloads/hru-delin-v7.tar.bz2
# add bin directory to your PATH variable
cd hru-delin
echo 'export PATH='`pwd`/bin':$PATH' >> ~/.bashrc
That's it! From now on, every new terminal will be able to run HRU delin. To check if everything is fine, open a new terminal and type
which hru-delin_step1.sh
It should display something like /home/myname/hru-delin/bin/hru-delin_step1.sh. If not, there's something wrong.

Usage
=====
=====

Basic
=====
Create a config file (.cfg) the same way you did with HRU delin v5 and previous versions. You can name it as you wish.
If you are in the same directory than the config file and its name is my_hrudelin_project.cfg, you can run:
hru-delin_step1.sh my_hrudelin_project.cfg
hru-delin_step2.sh my_hrudelin_project.cfg
hru-delin_step3.sh my_hrudelin_project.cfg
hru-delin_step4.sh my_hrudelin_project.cfg
to launch each HRU delin step.
You can also do it with:
hru-delin_all-steps.sh my_hrudelin_project.cfg
If you name your config file hrudelin_config.cfg, and you're placed in the same directory, you can call HRU delin scripts without arguments:
hru-delin_all-steps.sh

Intermediate
============
Multiple config files for one dataset
If you want to try multiple options and configuration for the same dataset, there is no need to duplicate the data set (specially if it's big). You can write multiple .cfg files that use this dataset.
Be carefull to write the results in different places/directories to make sure you don't loose track of what is produced by your different runs.

Start a run from partial results
If someone successfully ran step1 and step2 but wants to run step3 and step4 on another machine because it requires more CPU power, it is perfectly possible. The only directory that one needs to copy with the dataset is the intermediate results directory.
[dir_out]
files:my_intermediate_results
No need to copy grass_db and tmp directories.
On the destination machine, just run step3 and it will use intermediate results.

Advanced
========
Working directory
You don't need to be in the same directory than the config file, so you can run things like:
hru-delin_all-steps.sh ./projects/hydro/my_hrudelin_project.cfg
and HRU delin will run just fine, producing outputs next to the config file.

Number of parallel process
==========================
You can set the number of parallel process with -p option in all launch scripts. HRU delin will use the number of cores by default. You might want to use a smaller number to avoid using all your computer's resources.
Examples:
# run all steps with 2 parallel processes
hru-delin_all-steps.sh my_hrudelin_project.cfg -p 2
# run step4 with 8 parallel processes
hru-delin_step4.sh my_hrudelin_project.cfg -p 8

Multiple executions
==================
As HRU delin builds a specific grass environment each time it is launched, you can run it multiple times simultaneously. Each HRU delin running instance won't mess with the others.
For example, let's consider you have 8 cores and you want HRU delin to only use half of your processor's resources:
# in one terminal
hru-delin_all-steps.sh projects/rhone/rhone.cfg -p 2
# and in another one
hru-delin_all-steps.sh projects/danube/danube.cfg -p 2
This way you'll have one HRU delin using 2 cores and another one using 2 cores as well.

CircleKill
==========
ZirkelKill (Java) has been translated to Python3. It is included as a module and used by HRU delin.
You can also use it as a standalone program if you want. It can be called like a binary and takes 2 arguments:

the input path (.par file)
the output path prefix

Example:
/path/to/hru-delin/modules/circleKill.py myFile.par fixedFile
This will write fixedFile.par and fixedFile.dbf.