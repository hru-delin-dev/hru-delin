#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def isint(value):
    try:
        int(value)
        return True
    except ValueError:
        return False



print('------------- Export HRU and reach into OpenFLUID format domain.fluidx -------------')
# export in fluidx the topology of HRU and reach
        
# open hru.par
#skip 5 line
#read first column (cat) and column 13 (to_poly) and column 14 (to_reach)
#test if column 12 and colum 13 not Null or not 0.0
fo=open('domain.fluidx',"w")
fo.write("<?xml version=\"1.0\" standalone=\"yes\"?>\n")
fo.write("<openfluid>\n")
fo.write(" <domain>\n")
fo.write("  <definition>\n")
#for loop for each class unit
#read hru.par
hru_file=open("hru.par",'r')
hru_Lines=hru_file.readlines()
for i, line in enumerate( hru_Lines):
    if i>4:
        stripped_line = line.strip()
        line_list = stripped_line.split()
        if isint(line_list[0]):
                    
            fo.write(" <unit class=\"HRU\" ID=\"%s\" pcsorder=\"1\">\n" %(line_list[0]))
            # get the length of list
            if len(line_list)>12:
                if float(line_list[12])>0:
                    fo.write(" <to class=\"HRU\" ID=\"%i\" />\n" %(int(float(line_list[12]))))
            if len(line_list)>13:        
                if float(line_list[13])>0:
                    fo.write(" <to class=\"Reach\" ID=\"%s\" />\n" %(int(float(line_list[13]))))    
            fo.write(" </unit>\n")                                                      
hru_file.close()
#for loop for each class unit
#read reach.par
reach_file=open("reach.par",'r')
reach_Lines=reach_file.readlines()
for j, line in enumerate( reach_Lines):
    if j>4:
        stripped_line = line.strip()
        line_list = stripped_line.split()
        if isint(line_list[0]):
                    
            fo.write(" <unit class=\"Reach\" ID=\"%s\" pcsorder=\"1\">\n" %(line_list[0]))
            if float(line_list[1])>0:
                fo.write(" <to class=\"Reach\" ID=\"%s\" />\n" %(line_list[1])) 
            fo.write(" </unit>\n")                                                      
reach_file.close()
        
fo.write("  </definition>\n")
fo.write("  <calendar>\n")
fo.write("  </calendar>\n")
fo.write(" </domain>\n")
fo.write("</openfluid>\n")
fo.close()


