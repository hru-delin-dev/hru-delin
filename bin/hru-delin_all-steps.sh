#!/bin/bash
############################################################################
#
# MODULE:       hru-delin_all-steps.sh
# AUTHOR(S):    Julien Veyssier
# 
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################

MYPATH=`readlink -f $0`
MYDIR=`dirname $MYPATH`
echo $MYDIR

# very simple, we just call all steps scripts with same arguments we got here
$MYDIR/hru-delin_step1.sh "$@"
$MYDIR/hru-delin_step2.sh "$@"
$MYDIR/hru-delin_step3.sh "$@"
$MYDIR/hru-delin_step4.sh "$@"
