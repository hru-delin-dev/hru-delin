//
// Created by tlabrosse on july 2022.
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#include "File.h"

namespace gateway {

	File::File(const std::string &path, const std::string &name) : path(path), name(name) {}
	File::~File() {}

	const std::string &File::getPath() const {
		return path;
	}
	const std::string &File::getName() const {
		return name;
	}
} // gateway