//
// Created by tlabrosse on july 2022.
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#ifndef GATEWAY_GATESTUB_H
#define GATEWAY_GATESTUB_H

#include "../lib/json.hpp"
#include "ExecFile.h"

namespace gateway {

class GateStub {
private:
	ExecFile *execFile;
	OutputFile *output;
	std::string jsonLine;

	int fillFromJson(const std::string& jsonLine);
public:
	GateStub(const std::string& jsonLine);

	ExecFile *getExecFile() const;
	OutputFile *getOutput() const;

	const std::string &getJsonLine() const;
};
} // gateway

#endif //GATEWAY_GATESTUB_H
