//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#ifndef CPPGATE_ARGUMENT_H
#define CPPGATE_ARGUMENT_H

#include <iostream>
#include <map>
#include <string>
#include <utility>

#include "lib/json.hpp"

class Argument {
protected:
    std::string name;

public:
    explicit Argument(std::string name) :name(name) {}

    const std::string &getName() const {
        return name;
    }

    virtual Argument* getArgument(std::string name) = 0;

    virtual std::string serialize() const = 0;

    virtual void display() const = 0;
};


#endif //CPPGATE_ARGUMENT_H
