//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#include "Dictionary.h"
using json = nlohmann::json;


Dictionary::Dictionary(std::string name) : Argument(name){}


void Dictionary::addArgument(Argument* argument) {
    this->value.push_back(argument);
}
void Dictionary::addParameter(std::string name, std::string value) {
    Parameter* para = new Parameter(name, value);
    this->value.push_back(para);
}

Argument* Dictionary::getArgument(std::string name) {
    if(this->name == name) {
        return this;
    }

    for(auto argument : this->value) {
        Argument* arg = argument->getArgument(name);
        if (arg != nullptr)
            return arg;
    }
    return nullptr;
}
Parameter* Dictionary::getParameter(std::string name) {
    for(auto argument : this->value) {
        if(typeid(argument) == typeid(Parameter))
            if (argument->getName() != name)
                return dynamic_cast<Parameter *>(argument);
    }
    return nullptr;
}

std::string Dictionary::serialize() const {
    std::string output = R"({"Dictionary": { "name": ")" + this->name + R"(", "value" : [)";

    for(int k = 0; k < this->value.size(); k++) {
        output += this->value[k]->serialize();
        if(k != this->value.size()-1)
            output += ", ";
    }

    output += "]}}";
    return output;
}
void Dictionary::deserialize(std::string jsonLine) {
    auto data = json::parse(jsonLine);

    this->name = data["name"];
    auto value_list = data["value"];

    for(auto val : value_list) {
        if(val.contains("Dictionary")) {
            Dictionary* dictionary = new Dictionary("");
            dictionary->deserialize(val["Dictionary"]);

            this->value.push_back(dictionary);
        }
        else if(val.contains("Parameter")) {
            Parameter* parameter = new Parameter(val["Parameter"]["name"], val["Parameter"]["value"]);

            this->value.push_back(parameter);
        }
    }
}

void Dictionary::display() const {
    std::cout << this->serialize() << std::endl;
}
