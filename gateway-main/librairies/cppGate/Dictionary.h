//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#ifndef CPPGATE_DICTIONARY_H
#define CPPGATE_DICTIONARY_H

#include <vector>
#include "Parameter.h"

class Dictionary: public Argument {
private:
    std::vector<Argument*> value;

public:
    explicit Dictionary(std::string name);

    void addArgument(Argument* argument);
    void addParameter(std::string name, std::string value);

    Argument* getArgument(std::string name) override;
    Parameter* getParameter(std::string name);

    std::string serialize() const override;
    void deserialize(std::string jsonLine);

    void display() const override;
};


#endif //CPPGATE_DICTIONARY_H
