//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#include <fstream>
#include "OutputFile.h"

using json = nlohmann::json;

OutputFile::OutputFile(std::string path, std::string name) : File(path, name) {}

std::string OutputFile::read() const {
    if(actif) {
        if(this->name != "" && this->path != "") {
            std::ifstream file (this->path + this->name);
            if(!file.is_open()) {
                return "{}";
            }

            std::string line, output = "";
            while ( getline (file,line) )
            {
                output += line;
            }
            file.close();

            return output;
        }
    }
    return "{}";
}
Dictionary* OutputFile::readAsDictionary() const {
    if(actif) {
        auto data = json::parse(this->read());
        Dictionary* dictionary_output = new Dictionary("outputFile");

        Dictionary* outputs = new Dictionary("Outputs");
        for(auto output : data["Outputs"]) {
            Dictionary* dico = new Dictionary(output["Dictionary"]["name"]);
            dico->deserialize(output["Dictionary"]);
            outputs->addArgument(dico);
        }

        return dictionary_output;
    }
    return nullptr;
}

void OutputFile::writeOutput(Dictionary dictionary) {
    auto data = json::parse(this->read());

    data["Outputs"].push_back(dictionary.serialize());

    std::string jsonLine = data.dump();
    std::ofstream file (this->path + this->name, std::ios::trunc);
    file << jsonLine;
    file.close();
}


std::string OutputFile::serialize() const {
    if(this->actif)
        return  R"({"OutputFile": {"path": ")" + this->path +
                R"(", "name": ")" + this->name +
                R"("}})";

    return "";
}

void OutputFile::displayContent() const {
    std::cout << json::parse(this->read()).dump(1) << std::endl;
}
void OutputFile::display() const {
    std::cout << this->serialize() << std::endl;
}


