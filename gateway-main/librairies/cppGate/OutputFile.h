//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#ifndef CPPGATE_OUTPUTFILE_H
#define CPPGATE_OUTPUTFILE_H

#include "File.h"
#include "Dictionary.h"

class OutputFile: public File {
public:
    OutputFile(std::string path, std::string name);

    std::string read() const;
    Dictionary* readAsDictionary() const;

    void writeOutput(Dictionary dictionary);

    std::string serialize() const override;

    void displayContent() const;
    void display() const override;
};


#endif //CPPGATE_OUTPUTFILE_H
