//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#include "Parameter.h"

#include <utility>

Parameter::Parameter(std::string name, std::string value): Argument(name), value(std::move(value)) {}

Argument *Parameter::getArgument(std::string name) {
    if(this->name == name) {
        return this;
    }
    return nullptr;
}

int Parameter::getValueAsInt() const {
    return stoi(this->value);
}

int Parameter::getValueAsFloat() const {
    return stof(this->value);
}

int Parameter::getValueAsDouble() const {
    return stod(this->value);
}

std::string Parameter::serialize() const {
    std::string output = R"({"Parameter": { "name": ")" + this->name + R"(", "value": ")" + this->value + R"("}})";
    return output;
}

void Parameter::display() const {
    std::cout << this->serialize() << std::endl;
}