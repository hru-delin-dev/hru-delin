//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#ifndef CPPGATE_PARAMETER_H
#define CPPGATE_PARAMETER_H

#include <utility>

#include "Argument.h"

class Parameter : public Argument{
private:
    std::string value;

public:
    Parameter(std::string name, std::string value);

    Argument* getArgument(std::string name) override;

    int getValueAsInt() const;
    int getValueAsFloat() const;
    int getValueAsDouble() const;

    std::string serialize() const override;

    void display() const override;
};


#endif //CPPGATE_PARAMETER_H
