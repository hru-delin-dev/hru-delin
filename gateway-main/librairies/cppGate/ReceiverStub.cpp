//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#include "ReceiverStub.h"
using json = nlohmann::json;

ReceiverStub::ReceiverStub(int argc, char *argv[]): Stub(nullptr) {
    this->readArguments(argc, argv);
}

std::string ReceiverStub::readArguments(int argc, char *argv[]) {
    if(argc >= 1) {
        std::vector<std::string> args;
        for(int k = 0; k < argc; k++) {
            args.push_back(std::string(argv[k]));
        }

        return args[1];
    }

    this->actif = false;
    this->outputFile->setInactif();
    return "";
}

void ReceiverStub::deserialize(std::string jsonLine) {
    if(this->actif) {
        auto data = json::parse(jsonLine);

        this->outputFile = new OutputFile(data["OutputFile"]["path"], data["OutputFile"]["name"]);

        auto dictionaries_json = data["Dictionaries"];

        for(auto dictionary_json : dictionaries_json) {
            Dictionary* dictionary = new Dictionary("");
            dictionary->deserialize(dictionary_json["Dictionary"]);

            this->dictionaries.push_back(dictionary);
        }
    }
}
