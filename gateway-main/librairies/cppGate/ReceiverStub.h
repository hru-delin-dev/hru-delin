//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#ifndef CPPGATE_RECEIVERSTUB_H
#define CPPGATE_RECEIVERSTUB_H

#include "Stub.h"

class ReceiverStub: public Stub {
private:
    bool actif = true;

public:
    ReceiverStub(int argc, char *argv[]);

    std::string readArguments(int argc, char *argv[]);

    void deserialize(std::string jsonLine);
};


#endif //CPPGATE_RECEIVERSTUB_H
