//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#include "Stub.h"

#include <utility>

Stub::Stub(OutputFile* outputFile) {
    if(outputFile == nullptr) {
        outputFile = new OutputFile("","");
    }
    this->outputFile = outputFile;
    this->actif = true;
}

Argument *Stub::findArgumentWithName(std::string name) {
    if(this->actif) {
        Argument* arg = nullptr;
        for(auto dictionary : this->dictionaries) {
            arg = dictionary->getArgument(name);
            if(arg != nullptr)
                return dictionary;
        }
    }
    return nullptr;
}
Dictionary *Stub::findDictionaryWithName(std::string name) {
    if(this->actif)
        for(auto dico : this->dictionaries)
            if(dico->getName() == name)
                return dico;
    return nullptr;
}
Argument *Stub::getArgument(std::string name) {
    if(this->actif)
        return this->findArgumentWithName(std::move(name));

    return nullptr;
}

std::string Stub::displayDictionaries() const{
    if(actif) {
        std::string output = "{";

        for(int k = 0; k < this->dictionaries.size(); k++) {
            output += this->dictionaries[k]->serialize();
            if(k != this->dictionaries.size() -1)
                output += ", ";
        }
        output += "}";

        std::cout << output << std::endl;
        return output;
    }
    return "";
}
std::string Stub::displayOutputFile() const{
    if(actif) {
        std::string output = outputFile->serialize();

        std::cout << output << std::endl;
        return output;
    }
    return "";
}
std::string Stub::displayAll() const{
    if(actif) {
        std::string output = this->displayOutputFile() + this->displayDictionaries();

        std::cout << output << std::endl;
        return output;
    }
    return "";
}

OutputFile *Stub::getOutputFile() const {
    return outputFile;
}
const std::vector<Dictionary *> &Stub::getDictionaries() const {
    return dictionaries;
}

void Stub::setOutputFile(OutputFile *outputFile) {
    Stub::outputFile = outputFile;
}
void Stub::addDictionary(Dictionary *dictionary) {
    this->dictionaries.push_back(dictionary);
}


