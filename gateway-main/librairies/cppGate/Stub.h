//
// Created by tlabrosse on july 2022
// licence : GNU lgpl
// you can contact me at : theo.labt@gmail.com
//

#ifndef CPPGATE_STUB_H
#define CPPGATE_STUB_H

#include <vector>
#include "OutputFile.h"
#include "Dictionary.h"


class Stub {
protected:
    OutputFile* outputFile;
    std::vector<Dictionary*> dictionaries;
    bool actif;

public:
    Stub(OutputFile* outputFile = nullptr);

    Argument* findArgumentWithName(std::string name);
    Dictionary* findDictionaryWithName(std::string name);
    Argument* getArgument(std::string name);

    OutputFile *getOutputFile() const;
    const std::vector<Dictionary *> &getDictionaries() const;

    void setOutputFile(OutputFile *outputFile);
    void addDictionary(Dictionary* dictionary);

    std::string displayDictionaries() const;
    std::string displayOutputFile() const;

    virtual std::string displayAll() const;
};


#endif //CPPGATE_STUB_H
