#
# Created by tlabrosse on july 2022.
# licence : GNU lgpl
# you can contact me at : theo.labt@gmail.com
#

import json
from abc import abstractmethod
from .serializable import Serializable


class Argument(Serializable):
	def __init__(self, name):
		self.name = name
		self.value = None

	@abstractmethod
	def serialize(self) -> dict:
		pass

	def display(self):
		pass


class Parameter(Argument):
	def __init__(self, name, value):
		super().__init__(name)
		self.value = value

	def display(self) -> str:
		json_line = json.dumps(self.serialize(), indent=2)
		print(json_line)
		return json_line

	def getArgument(self, name: str) -> "Argument | None":
		if self.name == name:
			return self
		return None

	def getValueAsInt(self):
		return int(self.value)

	def getValueAsFloat(self):
		return float(self.value)

	def getValueAsList(self):
		return list(json.loads(self.value))

	def serialize(self) -> dict:
		return {
			"Parameter": {
				"name": self.name,
				"value": self.value
			}
		}


class Dictionary(Argument):
	def __init__(self, name):
		super().__init__(name)
		self.value = []

	def addParameter(self, name: str, value):
		"""
		This function will add a parameter to the dictionary.

		Parameters
		----------
		name : str
			The name of the parameter

		value
			The value of the parameter
		"""
		self.addArgument(Parameter(name, str(value)))

	def addArgument(self, argument: Argument) -> None:
		"""
		This function will add an argument to the Dictionary.

		Parameters
		----------
		argument : Argument
			The argument to add

		Raises
		------
		TypeError
			If the given parameter is not of Argument type
		"""
		if isinstance(argument, Argument):
			self.value.append(argument)
		else:
			raise TypeError("parameter needs to inherits from Argument")

	def getArgument(self, name: str) -> "Argument | None":
		"""This function will return an argument if it exists with the given name.
		If no argument exists with the given name, it will return None.

		Parameters
		----------
		name : str
			The name of the argument

		Returns
		-------
		Argument, None
			The argument with the right name, or nothing
		"""
		if self.name == name:
			return self

		for argument in self.value:
			arg = argument.getArgument(name)
			if arg is not None:
				return arg

		return None

	def getParameter(self, name: str) -> "Parameter | None":
		"""
		This function will return a parameter if it exists with the given name.
		If no parameter exists with the given name, it will return None.

		Parameters
		----------
		name : str
			The name of the parameter

		Returns
		-------
		Parameter, None
			The parameter with the right name, or nothing
		"""
		for argument in self.value:
			arg = argument.getArgument(name)
			if arg is not None:
				if type(arg) is Parameter:
					return arg

		return None

	def display(self) -> str:
		"""
		This function will print and return a string representation of the dictionary.

		Returns
		-------
		str
			The string representation of the dictionary
		"""
		json_line = json.dumps(self.serialize(), indent=2)
		print(json_line)
		return json_line

	def serialize(self) -> dict:
		dico = {
			"Dictionary": {
				"name": self.name
			}
		}
		value = []

		for val in self.value:
			value.append(val.serialize())
		dico["Dictionary"]["value"] = value

		return dico

	def deserialize(self, dico: dict) -> None:
		self.name = dico["name"]

		value_json = dico["value"]

		for val in value_json:
			if "Dictionary" in val:
				dictionary = Dictionary(val["Dictionary"]["name"])
				dictionary.deserialize(val["Dictionary"])

				self.value.append(dictionary)
			elif "Parameter" in val:
				parameter = Parameter(val["Parameter"]["name"], val["Parameter"]["value"])
				self.value.append(parameter)
