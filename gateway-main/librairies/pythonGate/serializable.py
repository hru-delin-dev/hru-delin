#
# Created by tlabrosse on july 2022.
# licence : GNU lgpl
# you can contact me at : theo.labt@gmail.com
#

from abc import ABC, abstractmethod


class Serializable(ABC):
    @abstractmethod
    def serialize(self) -> dict:
        return {}
