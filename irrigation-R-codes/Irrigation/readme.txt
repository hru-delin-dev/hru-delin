�HRUs_culture.csv : Fichier comprenant :
	- colonne 1 : le num�ro des HRUs
	- colonne 2 : le type de culture
Dans un premier temps, les cantons ont �t� s�lectionn�s en fonction du ratio SAUirr/Scanton. Le canton est irrigu� si le ratio est > 3% pour l'ensemble des cultures sauf le maraichage (1% pour le maraichage) 
Un croisement a ensuite �t� effectu� entre HRUs agricoles et cantons irrigu�s. Nous avons retenu uniquement les HRUs pour lesquelles la surface de la HRU est � au moins 50% sur le canton.
La culture dominante de la SAUirr du canton a �t� attribu�e � chaque HRU agricole pr�sente sur le canton.

�HRUs_culture_test.csv : Fichier identique au fichier HRUs_culture.csv pr�sent� ci-dessus avec en plus une colonne indiquant le canton auquel est rattach� la HRU
Ce fichier est issu du code R faisant les post-traitements.

�irrigation_table.csv : Fichier faisant la liaison entre cultures (pr�sentes dans HRUs_culture) et code pr�sents dans le landuse

�Bilan_irrigation.xlsx
	- Code Canton : Code du canton
	- SAUirr_sur_Scanton_en_pourcents : Rapport SAUirr / Surface Canton (en %)
	- HRUs_irriguees : Surface des HRUs irrigu�es pr�sentes sur le canton (en m2)
	- Surface_Canton : Surface du canton (en m2)
	- Pourc_canton_irrigue_HRUs	: Rapport HRUs_irriguees/Surface_Canton (en %) 
	- DOM_SAU_IR : Culture dominante sur le canton
	- Rapport_SAUirrigRGA_HRUirrig	: Rapport SAUirr / HRUs_irriguees (-)
	
�Bilan_simulation_irrigation.txt
Fichier dans lequel sont �crits les r�sultats des analyses de simulation avec irrigation (demande calcul�e, pr�l�vement r�el, pr�l�vement agence sur les p�riodes 1987-2007 et 2008-2012) men�es avec le code Analyse_Irrigation.r

RGACultures2010_Cantons_BVRhone_sanssecret_20131001.xlsx
Fichier RGA par canton sans secret statistique

Dans le dossier Resultats :
Fichiers comportant les r�sultats des simulations pr�sent�es dans le rapport MDR de novembre 2015.
Simulation avec et sans dose sur la p�riode 1987 - 2007 (demande corrig�e ou non)



Dans le dossier Shapes :

�Cantons_irrigues.shp : Couche comprenant les cantons irrigu�s finalement retenus 
Les cantons ont �t� s�lectionn�s en fonction du ratio SAUirr/Scanton. 
Le canton est irrigu� si le ratio est > 3% pour l'ensemble des cultures sauf le maraichage (1% pour le maraichage).
	- CODE_CAN_1 : Code du canton
	- NOM_CANTON : Nom du canton
	
	- POURC_IRR : SAUirr / (SAUirr + SAU non_irr)
	- SAU_TOT : SAU totale (en ares) = SAUirr + SAU non irr
	- DOM_SAUTOT : Culture dominante sur la SAU totale
	- P_DOM_TOT : Pourcentage de pr�sence de la culture dominante (Surf culture dominante / SAU totale)
	- SOM_S_TOT : Somme de Superficie totale (en ares)
	- SOMSNO_SAU : Somme de Superfice non SAU (en ares)
	- SAU_IRR : SAU irrigu�e (en ares)
	- DOM_SAU_IR :  Culture dominante sur la SAU irrigu�e
	- PDOMSAUIRR : Pourcentage de pr�sence de la culture dominante (Surf culture dominante / SAU irrigu�e)
	- SAUNOIRR : SAU non irrigu�e (en ares)
	- DOMSAUNOIR : Culture dominante sur la SAU non irrigu�e
	- PDOMSAUNOI : Pourcentage de pr�sence de la culture dominante (Surf culture dominante / SAU non irrigu�e)
	- P_IRR_SAU : SAUirr / (SAUirr + SAU non_irr)
	- SAUIRR_SCA : Ratio surface irrigu�e / surface canton
	- AIRE_TOTAL : Surface du canton pr�sente sur le bassin du Rh�ne (en m�)
	- SCANT : Surface totale du canton (en m�)
	- SCANTINBV : Surface du canton pr�sente sur le bassin du Rh�ne (en m�)
	- RTCANT : Ratio SCANTINBV / SCANT
	- IRRPROPSAU : SAU irr / Scanton (en %)
	- IRRPROPHRU : Somme HRUs irr / Scanton (en %)
	- PHRU_PSAU : Ratio IRRPROPHRU / IRRPROPSAU = Somme HRUs irr / SAU irr
	
�CantonsModif_BVRhone_IRR.shp
Couche cantons utilis�es pour int�grer le RGA et s�lectionner les cantons irrigu�s	

�hrus_irriguees_sur_Rhone.shp : Couche comprenant les HRUs irrigu�es finalement retenues
Les HRUs ont �t� retenues si elles sont pr�sentes � au moins 50% de leurs surfaces sur le canton irrigu� auquel elles sont rattach�es
La table attributaire comporte uniquement les HRUs irrigu�es.
Elle correspond aux donn�es pr�sentes dans le fichier hrus.par. 
La derni�re colonne, CODE_CAN_1, correspond au canton auquel les HRUs sont rattach�es 

�Simu_irrigation.shp : Couche dans laquelle sont �crits les r�sultats des simulations en sortie du code Analyse_Irrigation.r
	- CODE_CAN_1 : Code du canton
	- NOM_CANTON : Nom du canton
	- ZONE_ETUDE : Pour rassemblement des cantons : 1 = Rh�ne, 2 = Durance, 3 = Sa�ne)
	- POURC_IRR : SAUirr / (SAUirr + SAU non_irr)
	- SAU_IRR : SAU irrigu�e (en ares)
	- DOM_SAU_IR :  Culture dominante sur la SAU irrigu�e
	- PDOMSAUIRR : Pourcentage de pr�sence de la culture dominante (Surf culture dominante / SAU irrigu�e)
	- P_IRR_SAU : SAUirr / (SAUirr + SAU non_irr)
	- SAUIRR_SCA : Ratio surface irrigu�e / surface canton
	- AIRE_TOTAL : Surface du canton pr�sente sur le bassin du Rh�ne (en m�)
	- SCANT : Surface totale du canton (en m�)
	- SCANTINBV : Surface du canton pr�sente sur le bassin du Rh�ne (en m�)
	- RTCANT : Ratio SCANTINBV / SCANT
	- IRRPROPSAU : SAU irr / Scanton (en %)
	- IRRPROPHRU : Somme HRUs irr / Scanton (en %)
	- PHRU_PSAU : Ratio IRRPROPHRU / IRRPROPSAU = Somme HRUs irr / SAU irr
	- D1,D2,D3,D4... : respectivement demande calcul�e et demande calcul�e corrig�e par le rapport SAUirr/SHRU sur les p�riodes 1987-2007 (1 et 2) et 2008-2012 (3 et 4)
	- T1,T2,T3,T4... : respectivement transfert calcul� et transfert calcul� corrig� par le rapport SAUirr/SHRU sur les p�riodes 1987-2007 (1 et 2) et 2008-2012 (3 et 4)
	- P1 et P3 : pr�l�vements agence sur 1987-2007 (P1) et 2008-2012 (P3) 
	
