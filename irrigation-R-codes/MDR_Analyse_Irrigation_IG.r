########################################################################################
####### Comparaison des DEMANDE et TRANSFERTS modélisés aux PRELEVEMENTS AERMC #########
########################################################################################
# WARNING : since 17/05/2022, now using .dbf files in "user configuration" instead of .shp files
# the objectif being to not have to use maptools anymore


library(rgeos)
library(foreign)
library(sp)
library(raster)

source('lib/utilitaire_irrigation.R')


# *** CONFIGURATION UTILISATEUR ***
# ---------------------------------

# config='25MPS_Aleatoir'    # == Nom_simu
# chemin="~/DATA/SIG_MDR/irrigation/shape_AleatoirIrrig/" # chemin shape
# shp_file <- 'AleatoirIrrig_hrus_decoupees.shp'


#test3
config <- 'test4_MA'   #== Nom_simu
chemin <- '/home/tlabrosse/Bureau/maestro/irrigation-R-codes/Irrigation/Shapes/' # chemin shape
shp_file <- 'hrus_irriguees_sur_Rhone.dbf'

#
# config='25MPS_surest'    # == Nom_simu
# chemin="~/DATA/SIG_MDR/irrigation/shape_HRUs_Francois/" # chemin shape
# shp_file='hrus_irriguees_decoupees.shp'

chemin_sortie <- '/home/tlabrosse/Bureau/maestro/irrigation-R-codes/resultats OUT/'
pdfname <- paste0("/home/tlabrosse/Bureau/maestro/irrigation-R-codes/resultats OUT/Comparaison_Irrig_", config, ".pdf")


# *** TRAITEMENT des SORTIES DU MODELE ***
# ----------------------------------------

Nom_simu <- config

HRULoop <- ReadLoopDaily(paste0(chemin_sortie, Nom_simu, '/'), "HRULoop.dat", TRUE)
Dates <- HRULoop$dates

# Code canton des HRUs irriguées
HRUs <- read.dbf(paste0(chemin, shp_file))
regroup <- cbind(HRUs$CAT,HRUs$CODE_CAN_1) # join CAT - CODE_CAN_1 (deux id)
cantons <- regroup[order(regroup[,2]),] # sort
un_canton <- unique(cantons[,2]) # remove all duplicates 


# Calcul des chroniques journalières : cantonXXX_Demande, cantonXXX_Transfert, de dimensions : ncol=nb_HRUS_in_canton, nrows=Ntime
for (cant in un_canton){
      Nom <- paste0('canton', cant)
      HRUs_irr <- cantons[which(cantons[,2]== cant),1] # HRUs irriguées du canton "cant"

      Dem <- NULL
      Transf <- NULL
      for (k in HRUs_irr){
              Dem <- cbind(Dem,HRULoop$Data[which(HRULoop$Data[,1]==k),which(colnames(HRULoop$Data)=='irrigationDemand')]) # L
              Transf <- cbind(Transf,HRULoop$Data[which(HRULoop$Data[,1]==k),which(colnames(HRULoop$Data)=='irrigationTotal')]) # L
      }

assign(paste0(Nom, '_Demande'), Dem)
assign(paste0(Nom, '_Transfert'), Transf)
}

# Calcul des Demande et Transferts annuels interannuels par canton
Demande_interannuelle <- NULL
Transfert_interannuel <- NULL

for (cant in un_canton){
  Nom <- paste0('canton', cant)
  obj1 <- aggregateZoo(na.omit(zoo(apply(get(paste0(Nom, '_Demande')), 1, sum), Dates)), 'y', 'sum')/1000. # m3
  obj2 <- aggregateZoo(na.omit(zoo(apply(get(paste0(Nom, '_Transfert')), 1, sum), Dates)), 'y', 'sum')/1000. # m3
  z_dem <-  mean(obj1 [-c(1,2,24:28)] ) #Valeur sur 1987 - 2007
  z_transf <- mean( obj2 [-c(1,2,24:28)] ) #Valeur sur 1987 - 2007
  Demande_interannuelle <- rbind( Demande_interannuelle, z_dem)
  Transfert_interannuel <- rbind( Transfert_interannuel, z_transf)
}

irrig_interannuelle_simu <- cbind(canton=un_canton,demande=Demande_interannuelle,transfert=Transfert_interannuel)
rownames(irrig_interannuelle_simu) <- NULL
colnames(irrig_interannuelle_simu) <- c('canton','demande','tranfert')


# *** PRELEVEMENTS AERMC ***
# --------------------------
Prelev <- Prelev8182_1987_2007() # m3 . 81: GRAV ; 82 : non-grav
# Pour certains des cantons agricoles modélisés, les prélèvements sont nuls (eg le canton n'apparait pas dans les prélèvements de l'AERMC)
# => ajouter cette colonne aux prélèvements avec pour valeur 0
prelev <- NULL
for (cant in un_canton){
  if (length(Prelev[which(Prelev[,1] == cant)])>0) {
    prelev <- rbind(prelev,Prelev[which(Prelev[,1] == cant),2])
  } else {
    prelev <- rbind(prelev,0.)
  }
}
PrelevAll <- prelev # m3/yr
colnames(PrelevAll) <- 'PrelevAll'

Prelev <- Prelev82_1987_2007()
prelev <- NULL
for (cant in un_canton){
  if (length(Prelev[which(Prelev[,1] == cant)])>0) {
    prelev <- rbind(prelev,Prelev[which(Prelev[,1] == cant),2])
  } else {
    prelev <- rbind(prelev,0.)
  }
}
PrelevNonGrav <- prelev # m3/yr
colnames(PrelevNonGrav) <- 'PrelevNonGrav'

Prelev <- Prelev8182_2008_2012() #
prelev <- NULL
for (cant in un_canton){
  if (length(Prelev[which(Prelev[,1] == cant)])>0) {
    prelev <- rbind(prelev,Prelev[which(Prelev[,1] == cant),2])
  } else {
    prelev <- rbind(prelev,0.)
  }
}
PrelevAll_post2008 <- prelev # m3/yr
colnames(PrelevAll_post2008) <- 'PrelevAll_post2008'

# *** GRAPHES DE COMPARAISON PRELEV - TRANSFERTS ***
# --------------------------------------------------

# tous les cantons
comparaison <- cbind(irrig_interannuelle_simu, PrelevAll, PrelevNonGrav, PrelevAll_post2008)
save(comparaison,file= paste0('~/Documents/MDR/irrigation/RDATA/Comparaison_Irrig_', config, '.Rdata'))


# Cantons_Rhone <- c(101,117,118,119,120,140,518,717,722,724,1333,2602,2604,2607,2611,2613,2615,2616,2619,2621,2623,2625,2626,2628,2629,2632,2634,3006,3009, 3016, 3023,3026,3802,3807,3808,3815,3819,3822,3824,3825,3830,3837,3846,3853,4213,4233,6907,6924,6931,6937,6938,6944,6945,6948,6949,7405,8405,8406,8409,8413,8415,8416,8418,8423)
#
# Cantons_Durance <- c(410,413,414,416,419,420,421,427,429,430,505,509,512,515,516,522,523,524,1307, 1309,1312,1326,1327,1331,8319,8408,8411)
#
# Cantons_Saone <- c(102,126,135,2103,2114,2134,2138,3909,6905,6910,6925,7116,7151)

# seuls ceux présents à >99% sur notre domaine
Cantons_Rhone <- c(101,117,118,119,120,140,518,717,722,724,2602,2604,2607,2611,2613,2615,2616,2619,2621,2623,2625,2626,2628,2629,2632,2634,3006,3023,3026,3802,3807,3808,3815,3819,3822,3824,3825,3830,3837,3846,3853,4213,4233,6907,6924,6931,6937,6938,6944,6945,6948,6949,7405,8405,8406,8409,8413,8415,8416,8418,8423)
#
Cantons_Durance <- c(410,413,414,416,419,420,421,427,429,430,505,509,512,515,516,522,523,524,1326,1327,8319,8408,8411)
#
Cantons_Saone <- c(102,126,135,2103,2114,2134,2138,3909,6905,6910,6925,7116,7151)

# fichier à charger pour avoir les cultures dominantes par canton
canton_cult <- read.dbf('~/DATA/SIG_MDR/irrigation/shape_AleatoirIrrig_CultureNew/AleatoirIrrig_CN_hrus_decoupees.dbf')
canton_cult <- canton_cult[, c('CODE_CAN_1','LANDUSEID')]
canton_cult <- canton_cult[!duplicated(canton_cult$CODE_CAN_1),]


pdf(pdfname,paper <- "special",width=8,height=14)
layout(matrix(c(1,3,5,1,3,5,1,3,5,2,4,5),3,4))
par (pty="m")

# Rhone
mat_Rhone <- as.matrix(comparaison[which(comparaison[,1] %in% Cantons_Rhone),2:6])
cantonlist <- comparaison[which(comparaison[,1] %in% Cantons_Rhone),1]
culturelist <- substr(luid2cult(canton_cult[match(cantonlist,canton_cult$CODE_CAN_1),2]), start=1, stop=3)
row.names(mat_Rhone) <- paste(cantonlist, culturelist)
petits <- (which(mat_Rhone[, 3]/1000000<10))
barplot(t(mat_Rhone[petits,])/1000000., beside = TRUE, col = c("red", "blue", "green","forestgreen", "black"), legend.text = TRUE, main="RHONE : Demande,Transfert et Prélèvements AERMC sur 1987-2007", xlab='cantons', ylab='Mm3', ylim=c(0,10),border=NA,las=2, cex.names=0.65)
barplot(t(mat_Rhone[-petits,])/1000000., beside = TRUE, col = c("red", "blue", "green","forestgreen","black"), legend.text = TRUE, main="", xlab='cantons', ylab='Mm3', ylim=c(0,150),las=2, cex.names=0.65)

par (pty="m")
# Durance
mat_Durance <- as.matrix(comparaison[which(comparaison[, 1] %in%Cantons_Durance), 2:6])
cantonlist <- comparaison[which(comparaison[, 1] %in% Cantons_Durance), 1]
culturelist <- substr(luid2cult(canton_cult[match(cantonlist, canton_cult$CODE_CAN_1), 2]), start=1, stop=3)
row.names(mat_Durance) <- paste(cantonlist, culturelist)
petits <- (which(mat_Durance[, 3]/1000000<20))
barplot(t(mat_Durance[petits,])/1000000, beside = TRUE, col = c("red", "blue", "green","forestgreen", "black"), legend.text = TRUE, main="DURANCE : Demande,Transfert et Prélèvements AERMC sur 1987-2007", xlab='cantons', ylab='Mm3', las=2, ylim=c(0,20),cex.names=0.65)
barplot(t(mat_Durance[-petits,])/1000000, beside = TRUE, col = c("red", "blue", "green","forestgreen", "black"), legend.text = TRUE, main="", xlab='cantons', ylab='Mm3', las=2, ylim=c(0,150),cex.names=0.65)

par (pty="m")
# Saone
mat_Saone <- as.matrix(comparaison[which(comparaison[, 1] %in%Cantons_Saone), 2:6])
cantonlist <- comparaison[which(comparaison[, 1] %in% Cantons_Saone), 1]
culturelist <- substr(luid2cult(canton_cult[match(cantonlist, canton_cult$CODE_CAN_1), 2]), start=1, stop=3)
row.names(mat_Saone) <- paste(cantonlist, culturelist)
barplot(t(mat_Saone)/1000000, beside = TRUE, col = c("red", "blue", "green","forestgreen", "black"), legend.text = TRUE, main="SAONE : Demande,Transfert et Prélèvements AERMC sur 1987-2007", xlab='cantons', ylab='Mm3', las=2,cex.names=0.65)

graphics.off()



# *** ANALYSE PAR TYPE DE CULTURE et SOUS-BASSINS (code en cours d'écriture) ***
# ------------------------------------------------------------------------------

# -- Irrigation par hectare irrigué, pour détection des erreurs de surface ou pb de choix de méthode d'irrigation
culture_hru <- luid2cult(HRUs$LANDUSEID)

irrigarea_cant <- NULL # SAU irriguée par canton dans notre modélisation (proche valeurs du RGA)
culture_cant <- NULL
for (cant in un_canton){
  irrigarea_cant <- c(irrigarea_cant, sum(HRUs$AREA[which(HRUs$CODE_CAN_1 ==cant)])/10000.) #hectares
  culture_cant <- c(culture_cant, unique(culture_hru[which(HRUs$CODE_CAN_1 ==cant)]))
}

comparaison_surf <- cbind(un_canton, irrig_interannuelle_simu[, 2:3]%/%irrigarea_cant, PrelevAll%/%irrigarea_cant)

Rhone <- comparaison_surf[which(comparaison_surf[, 1] %in% Cantons_Rhone),]
cultures_Rhone <- culture_cant[which(un_canton %in% Cantons_Rhone )]

N <- length(unique(cultures_Rhone)) #==> 5 types de culture : "Mais"       "Prairies"   "Vergers"    "Vigne"      "maraichage"
culture_locale <- unique(cultures_Rhone)

pdfname <- paste0("~/Documents/MDR/irrigation/ComparaisonSurf_Rhone_", config, ".pdf")
pdf(pdfname,paper="special",width=8,height=14)
layout(matrix(1:N, N, 1))
par (pty="m")

for (cult in 1:N){
  mat <- as.matrix(Rhone[which(cultures_Rhone==culture_locale[cult]), 2:4])
  row.names(mat) <- Rhone[which(cultures_Rhone==culture_locale[cult]), 1]
  barplot(t(mat), beside = TRUE, col = c("red", "blue", "green"), legend.text = TRUE, main= paste0('Rhone , ', culture_locale[cult]), xlab='cantons', ylab='m3 / hectare', las=2)
}

graphics.off()
mat_Rhone <- as.matrix(comparaison[which(comparaison[, 1] %in% Cantons_Rhone), 2:5])
row.names(mat_Rhone) <- comparaison[which(comparaison[, 1] %in% Cantons_Rhone), 1]
petits <- (which(mat_Rhone[, 3]/1000000<10))
barplot(t(mat_Rhone[petits,])/1000000., beside = TRUE, col = c("red", "blue", "green","forestgreen"), legend.text = TRUE, main="RHONE : Demande,Transfert et Prélèvements AERMC sur 1987-2007", xlab='cantons', ylab='Mm3', ylim=c(0,10),border=NA,las=2, cex.names=0.75)
barplot(t(mat_Rhone[-petits,])/1000000., beside = TRUE, col = c("red", "blue", "green","forestgreen"), legend.text = TRUE, main="", xlab='cantons', ylab='Mm3', ylim=c(0,150),las=2)

########################################################################################
####### Ajout de la contrainte Q > 10% MA (Module Annuel) pour Prélèv Irrigation #######
########################################################################################

# fichiers pour extraction des MA :
simufile <- '~/JAMS/modeldata/J2K_Rhone_Barrages/output/BAR/'
filename <- 'ReachLoop.dat'

# le vieux et nouveau Reach.par
paramdir <- '~/JAMS/modeldata/J2K_Rhone_Irrigation/parameter/'
oldparfile <- 'reach.par'
newparfile <- 'reach_MA.par' # avec nouveau param MA

# code
rloop <- ReadLoopDaily(simufile, filename, FALSE)
MA <- NULL
Nbreach <- dim(rloop$Data)[1]/length(rloop$dates)
Nbtime <- length(rloop$dates)
Ntot <- dim(rloop$Data)[1]
for (i in (1:Nbreach)){ # 1 to 3075 reaches
  index <- seq(i, Ntot, Nbreach)
  chronique <- rloop$Data[index, 2]
  MA_tmp <- mean(chronique)
  MA <- c(MA, MA_tmp) # L/d
}

ID <- rloop$Data[1:Nbreach, 1]
MA <- rbind(ID, MA)

reaches <- Chargement_param(oldReachParfile, parfile)
order <- match(ID, reaches$V1) # l'ordre des simus est inversé par rapport à l'ordre du reach.par...
MA <- MA[, order]

newparamName <- "MA"
newparamVal <- round(MA[2,])
newparamUnit <- "L/d"
add_param(paramdir,oldparfile,newparfile,newparamName,newparamVal,newparamUnit)

########################################################################################
####### Impact de la paramétrisation MA sur les débits journaliers sur qques HRUS ###### (en cours)
########################################################################################

# ex : cantons (retenu) 3909 (reach6222) : forte baisse de la demande suite à introduction de cette paramétrisation.
simnewdir <- paste0(chemin_sortie, config, "/")
filename <- 'ReachLoop.dat'

simrefdir <- '~/JAMS/modeldata/J2K_Rhone_Natural_Hydrology/output/newREF/'

simolddir <- paste0(chemin_sortie, 'test3', "/")

myreach <- 6222

# code

# avec MA
rloopnew <- ReadLoopDaily(simnewdir, filename, FALSE)
runoffnew <- rloopnew$Data[which(rloopnew$Data[, 1]==myreach), 'simRunoff']
runoffnew <- xts(runoffnew, as.POSIXct(rloopnew$dates, format='%Y-%m-%d'))

# Hydro Nat
rloopref <- ReadLoopDaily(simrefdir, filename, FALSE)
runoffref <- rloopref$Data[which(rloopref$Data[, 1]==myreach), 'simRunoff']
runoffref <- xts(runoffref, as.POSIXct(rloopref$dates, format='%Y-%m-%d'))

# irrig sans MA
rloopold <- ReadLoopDaily(simolddir, filename, FALSE)
runoffold <- rloopold$Data[which(rloopold$Data[, 1]==myreach), 'simRunoff']
runoffold <- xts(runoffold, as.POSIXct(rloopold$dates, format='%Y-%m-%d'))

MA <- mean(runoffref)
MAts <- xts(rep(MA, length(runoffref)), as.POSIXct(rloopref$dates, format='%Y-%m-%d'))
MA10ts <- xts(rep(MA*.1, length(runoffref)), as.POSIXct(rloopref$dates, format='%Y-%m-%d'))

year <- '2009'
period <- paste0(year, '-05-01/', year, '-10-31')
plot(runoffref[period],ylim=c(0, MA*2), main="débit à l'aval du canton 3909 (CHEMIN, cult=mais, petit affluent", ylab='L/d')
lines(runoffnew[period],col=2)
lines(runoffold[period],col=4)
lines(MAts[period], col=3)
lines(MA10ts[period], col=3, lty=4)

legend("topright",legend=names(variablesfut),y.intersp = 1, lty= 1,bty="n",col = colors,xpd=NA,cex=0.8)

