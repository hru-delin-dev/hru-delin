library(R6)

File <- R6Class("File",
public=list(
  name = "",
  path = "",
  actif = TRUE,

  initialize = function(path, name) {
    stopifnot(is.character(name), length(name) == 1)
    stopifnot(is.character(path), length(path) == 1)

    self$actif=TRUE

    self$name = name
    self$path = path
  },

  display = function() {
    if(self$actif){
      json_line = toJSON(self$serialize())
      print(json_line)
      return(json_line)
    }
    return("")
  },

  serialize = function() {
    if(self$actif){
      return(list(
        "File" = list(
          "name" = self$name,
          "path" = self$path
        )
      ))
    }
    return(list())
  }
))



ExecFile <- R6Class("ExecFile", inherit = File,
public = list(
  cmd = "",
  initialize = function(path, name, cmd) {
    stopifnot(is.character(cmd), length(cmd) == 1)

    self$cmd = cmd
    super$initialize(path, name)
  },

  display = function() {
    if(self$actif){
      json_line = toJSON(self$serialize())
      print(json_line)
      return(json_line)
    }
    return("")
  },

  serialize = function() {
    if(self$actif){
      dico = super$serialize()

      dico$ExecFile = dico$File
      dico$File = NULL

      dico$ExecFile$cmd = self$cmd

      return(dico)
    }
    return(list())
  }
))



OutputFile <- R6Class("OutputFile", inherit = File,
public = list(
  initialize = function(path, name) {
    super$initialize(path, name)
  },

  display = function() {
    if(self$actif){
      json_line = toJSON(self$serialize())
      print(json_line)
      return(json_line)
    }
    return("")
  },

  serialize = function() {
    if(self$actif){
      dico = super$serialize()

      dico$OutputFile = dico$File
      dico$File = NULL

      return(dico)
    }
    return(list())
  },

  read = function() {
    if(self$actif){
      json_line = paste0(readLines(paste0(self$path, self$name)))
      line = fromJSON(json_line)

      return(line)
    }
    return("")
  },

  readAsDictionary = function() {
    if(self$actif){
      dictionary_dict = Dictionary$new("outputFile")

      file = self$read()
      output_dico = Dictionary$new("Outputs")
      for(i in seq_along(file$Outputs)) {
        dico = Dictionary$new(file$Outputs$Dictionary$name[[i]])
        dico$deserialize(file$Outputs$Dictionary$value[[i]])

        output_dico$addArgument(dico)
      }
      dictionary_dict$addArgument(output_dico)

      error_dico = Dictionary$new("Error")
      error_dico$addParameter("code", file$Error$code)
      error_dico$addParameter("description", file$Error$description)
      error_dico$addParameter("traceback", file$Error$traceback)

      dictionary_dict$addArgument(error_dico)

      check_dico = Dictionary$new("Check")
      for(i in seq_along(file$Check)) {
        dataFile_dico = Dictionary$new(file$Check$DataFile$fileName[[i]])
        dataFile_dico$addParameter("name", file$Check$DataFile$fileName[[i]])

        dataFile_dico$addParameter("nbMissing", file$Check$DataFile$nbMissing[[i]])
        if(!length( file$Check$DataFile$missing[[i]]) == 0)
          dataFile_dico$addParameter("missing", file$Check$DataFile$missing[[i]])

        dataFile_dico$addParameter("nbIncorrect", file$Check$DataFile$nbIncorrect[[i]])
        if(!length( file$Check$DataFile$incorrect[[i]]) == 0)
          dataFile_dico$addParameter("missing", file$Check$DataFile$incorrect[[i]])

        check_dico$addArgument(dataFile_dico)
      }

      dictionary_dict$addArgument(check_dico)
      return(dictionary_dict)
    }
    return(Dictionary$new(""))
  },

  displayContent = function() {
    if(self$actif){
      json_line = prettify(toJSON(self$read()))
      print(json_line)
      return(json_line)
    }
    return("")
  },

  writeOutput = function(dictionary) {
    if(self$actif){
      file = toJSON(self$read())

      to_add = paste0('"Outputs":[', improveSerialize(toJSON(dictionary$serialize())), ',')
      file = paste0(append(strsplit(file, '"Outputs":\\[')[[1]], to_add, after=1))

      write(prettify(file), paste0(self$path, self$name))
    }
    invisible(self)
  }

))
