# ==============================================================================
# By Theo L. intern at INRAE 
# CREATED on May 16, 2022
# 
# 
# ------------------------------------------------------------------------------
# the objective is to regroup the essential function of the MDR_irrigated project
# in an easy to maintain and well documented file
#
# most of the functions that are in this file are comming from another R source
# file, to keep track of those the "From" boxes indicate the name of the original 
# file
# ==============================================================================

library(zoo)
library(xts)



# ==========================
# ** From MDR_utilitaires **
# ==========================


# -------------------------------add_param--------------------------------------
# **** add an extra parameter to reach.par

# add_param <- function(inputdir, oldreachfile, newreachfile, newparamName, newparamVal, newparamUnit)
#      Adds a new parameter to the given reach file, with the given value and unit.
# Args:
#     inputdir: The input directory
#     oldreachfile: The old reach file
#     newreachfile: The new reach file
#     newparamName: The name of the new parameter
#     newparamVal: The value of the new parameter
#     newparamUnit: The unit of the new parameter

# The point of this function is to add a new parameter to an oldreachfile and create a newreachfile with the new parameter included. 
# The function takes the inputdir (input directory), oldreachfile, newreachfile, newparamName, newparamVal, and newparamUnit as arguments. 
# it then starts by finding the number of lines in the oldreachfile, then reads the header line and finds the line where the ID is located. 
# Then the function reads in the oldreachfile, adds the new paramName to the file, and creates a new Min, Max, and Unit file with the new parameter included. 
# Finally, the function writes the newreachfile with the new parameter included. 
# ------------------------------------------------------------------------------
add_param <- function(inputdir, oldreachfile, newreachfile, newparamName, newparamVal, newparamUnit) {
  
  nbLines <- skip_lines(inputdir,oldreachfile)
  headerReach <- readLines(paste0(inputdir, oldreachfile), n = nbLines)
  LinesNames <- which(substr(headerReach,1,2)=="ID")
  Names <- read.table(paste0(inputdir, oldreachfile), nr=1, skip=LinesNames-1)
  Names <- cbind(Names,newparamName)
  Min <- read.table(paste0(inputdir, oldreachfile), nr=1, skip=LinesNames)
  Min <- cbind(Min,0,0)
  Max <- read.table(paste0(inputdir, oldreachfile), nr=1, skip=LinesNames+1)
  Max <- cbind(Max,9999999,9999999)
  Unit <-  read.table(paste0(inputdir, oldreachfile), nr=1, skip=LinesNames+2)
  Unit <- cbind(Unit,newparamUnit)
  reach <- Chargement_param(inputdir,oldreachfile)
  reach <- cbind(reach,newparamVal)
  
  write.table (Names, paste0(inputdir, newreachfile), col.names=F, row.names=F, quote=F, sep='\t', append=F)
  write.table (Min, paste0(inputdir, newreachfile), col.names=F, row.names=F, quote=F, sep='\t', append=T)
  write.table (Max, paste0(inputdir, newreachfile), col.names=F, row.names=F, quote=F, sep='\t', append=T)
  write.table (Unit, paste0(inputdir, newreachfile), col.names=F, row.names=F, quote=F, sep='\t', append=T)
  write.table (reach, paste0(inputdir, newreachfile), col.names=F, row.names=F, quote=F, sep='\t', append=T)
}


# -------------------------------Chargement_param-------------------------------
# Chargement_param <- function(chemin,Name)
#   Loads all parameters from a given file.
# Args:
#   chemin: The path to the file
#   Name: The name of the file
# Returns:
#   The parameters as a data frame

# - The code is able to identify the line with the first values and skip the initial text lines
# Caveats: it may not work for files with less than 3 lines of data.
# ------------------------------------------------------------------------------
Chargement_param <- function(chemin, Name) { 
  # initialization
  k <- 0
  obj <- NULL; obj2 <- NULL; obj3 <- NULL
  
  # loop until we find a line with 3 numeric value
  while(length(na.omit(obj))==0 | length(na.omit(obj2))==0 | length(na.omit(obj3))==0) {
    
    obj <- as.numeric(read.table(paste0(chemin, Name), nrow=1, skip=k, colClasses="character"))[1]
    obj2 <- as.numeric(read.table(paste0(chemin, Name), nrow=1, skip=k+1, colClasses="character"))[1]
    obj3 <- as.numeric(read.table(paste0(chemin, Name), nrow=1, skip=k+2, colClasses="character"))[1]
    k <- k+1
  }
  
  # get the number of line to skip to get the data
  nbLines <- k - 1
  
  # load the data
  data <- read.table(paste0(chemin, Name), skip=nbLines)
  mycolnames <- apply(read.table(paste0(chemin, Name), nrow=1)[1,], 1, as.character)
  colnames(data) <- mycolnames
  
  return(data)
}


# -------------------------------write_new_paramfile----------------------------
# write_new_paramfile=<-(oldfile, newvalues ,newfile)
#      Writes the new combination of HRUs to the original parameter file.
# Args:
#     oldfile: The original parameter file
#     newvalues: A vector of the HRUs to write to the new parameter file
#     newfile: The new parameter file

# write a modified params file using the header of the old one 
# ------------------------------------------------------------------------------
write_new_paramfile <- function(oldfile, newvalues ,newfile) {
  # get the header
  nb_lines <- skip_lines(oldfile)  
  header <- readLines(oldfile, n = nb_lines)
  
  # write
  write.table(header, newfile, sep = '\t', col.names = F, row.names = F, quote = F)
  write.table(newvalues, newfile, col.names = F, row.names = F, quote = F, append = TRUE, sep = '\t')
} 


# -------------------------------skip_lines-------------------------------------
# skip_lines <- function(file)
#      Finds the number of lines to skip before the data starts in file.
# Args:
#     file: The file to be read
# Returns:
#      The number of lines to skip before the data starts in file
# ------------------------------------------------------------------------------
skip_lines <- function(file){
  k <- 0
  obj <- NULL; obj2 <- NULL; obj3 <- NULL
  while (length(na.omit(obj)) == 0 | length(na.omit(obj2)) == 0 | length(na.omit(obj3)) == 0) {
    
    obj <- as.numeric(read.table(file, nrow = 1, skip = k, colClasses = "character"))[1]
    obj2 <- as.numeric(read.table(file, nrow = 1, skip = k + 1, colClasses = "character"))[1]
    obj3 <- as.numeric(read.table(file, nrow = 1, skip = k + 2, colClasses = "character"))[1]
    
    k <- k + 1
  }
  return(k - 1)
}


# -------------------------------skip_lines-------------------------------------
# skip_lines <- function(chemin, Name)
#      Finds the number of lines to skip in order to reach the data in a file.
# Args:
#     chemin: The path to the file
#     Name: The name of the file
# Returns:
#      The number of lines to skip
# ------------------------------------------------------------------------------
skip_lines <- function(chemin, Name) {
  return(skip_lines(paste0(chemin, Name)))
}


# -------------------------------luid2cult--------------------------------------
# luid2cult <- function(vect_luid)
#      Converts a vector of J2000 culture codes to their corresponding name.
# Args:
#     vect_luid: A vector of J2000 culture codes
# Returns:
#      A vector containing the corresponding names

# The point of this function is to match a vector of numbers with a vector of strings. 
# ------------------------------------------------------------------------------
luid2cult <- function(vect_luid) {
  cultures <- c('Vigne', 'Mais', 'Tournesol', 'Blé dur', 'Maraichage', 'PdT', 'Vergers', 'Prairies', 'Protéagineux', 'Riz', "Jachère","Divers", "Industrielles")
  numJ2000_cultures <- 19:31
  res <- apply(as.matrix(vect_luid), 2, function(X){cultures[match(X, numJ2000_cultures)]})
  return(as.vector(res)) # Not sure this is necessary...
}

vec <- luid2cult(18:25)


# ===================================
# ** From readwritefunctions_J2000 **
# ===================================

# -------------------------------ReadLoopDaily----------------------------------
# ReadLoopDaily <- function(folder, file,filtre)
#      Reads a "daily file" and returns the data and the corresponding dates.
# Args:
#     folder: The folder where the file is located
#     file: The name of the file
#     filtre: A logical indicating whether to filter the data or not
# Returns:
#      A list containing the dates and the data

# 1. it starts by  reading the length of the first data block and the number of blocks in the file
# 2. then it reads the file's headers (column names)
# 3. finally it reads the data block by block, adding a day to the date at each block, until the end of the file
# ------------------------------------------------------------------------------
ReadLoopDaily <- function(folder, file, filtre) {
  # Open the file
  con <- file(paste0(folder, file))
  open(con)
  
  # Be careful as we remain in the same connection we must count the lines from the current line read (not from the beginning of the file)
  
  # Read the nb of elements (HRUs or Reaches) (length of the blocks)
  Lblocks <- read.table(con, nrows = 1, sep = "\t", skip = 1)
  Lblocks <- Lblocks[,3]
  
  # Get the nb of time steps fo the simulation (nb of blocks of the file)
  Nblocks <- read.table(con, nrows = 1, sep = "\t", skip = 1)
  Nblocks <- Nblocks[,3]
  
  # Get the col names (names of the simulated variables)
  if (filtre == T) {Colnames <- scan(con, what = "", nlines = 1, sep = "", skip = 3)} else  {Colnames <- scan(con, what = "", nlines = 1, sep = "", skip = 2)}
  #Colnames <- scan(con, what = "", nlines = 1, sep = "", skip = 2)
  
  # Get the start date of the simulation (we consider only the date -> daily time step)
  Datestart <- read.table(con, as.is = TRUE, nrows = 1, sep = "", skip = 3)
  Datestart <- Datestart[,2]
  if (filtre == T) {
    read.table(con, nrows = 1, sep = "\t")
    count <- length(Colnames)+1
    compt <- 0
    while (count == (length(Colnames)+1)) {
      obj <- read.table(con, nrows = 1, sep = "\t")
      count <- dim(obj)[2]
      compt <- compt + 1
    }
    Lblocks <- compt-1
    con<-file(paste0(folder, file))
    open(con)
    read.table(con, nrows = 1, sep = "\t", skip = 1)
    read.table(con, nrows = 1, sep = "\t", skip = 1)
    Colnames <- scan(con, what = "", nlines = 1, sep = "", skip = 3)
    # Get the start date of the simulation (we consider only the date -> daily time step)
    read.table(con, as.is = TRUE, nrows = 1, sep = "", skip = 3)
  }
  
  # Read the data
  # Initialisation of a matrix of the correct size to store the data
  # nrow = nb of time steps * nb of elts
  # ncol = nb of simulated variables
  ncol <-length(Colnames)
  Data <- matrix(nrow=Nblocks*Lblocks,ncol=ncol)
  # Loop on the nb of blocks
  for (i in 0:(Nblocks -1))
  {
    # Read the block of data
    # if i=0 (first block skip only 1 line)
    if(i==0)
      Datatemp <- read.table(con, nrows = Lblocks, sep = "", skip = 1, colClasses="numeric")
    # else skip 3 lines
    else
      Datatemp <- read.table(con, nrows = Lblocks, sep = "", skip = 3, colClasses="numeric")
    
    # Add the values to the matrix
    Data[(i*Lblocks+1):((i+1)*Lblocks),1:ncol] <- as.matrix(Datatemp)
  }
  
  # close the file
  close(con)
  
  # Add the colnames
  colnames(Data) <- Colnames
  # Create the corresponding vector of dates
  dates <- as.character(seq(from = as.Date(Datestart), length.out = Nblocks, by = "day"))
  
  # Return the vector of dates and the data as a list
  list(dates=dates, Data=Data)
}



# ========================
# ** From zoo_functions **
# ========================


# -------------------------------aggregateZoo-----------------------------------
# aggregateZoo <- function (z, timeStep, sumOrMeanFunction)
#      Aggregates the given zoo object over the given time step.
# Args:
#     z: A zoo object
#     timeStep: The time step over which to aggregate the zoo object
#          Possible values: ["dmy","my","m","sy","s"]
#     sumOrMeanFunction: The function to apply to the aggregated zoo object
#          Possible values: [sum,mean]
# Returns:
#      The aggregated zoo object

# The point of this R function is to aggregate data over different time steps. 
# The different time steps that are supported are "dmy", "my", "m", "sy", and "s". 
# For each time step, the function will either take the sum or mean of the data.
# ------------------------------------------------------------------------------
aggregateZoo <- function (z, timeStep, sumOrMeanFunction) {
  #Retourne un nouveau objet zoo aggr?g? sur le pas de temps timeStep
  #(["dmy","my","m","sy","s"]) en faisant la somme ou la moyenne ([sum,mean])
  
  if(timeStep == "dmy"){
    return (aggregate(z, time(z) - as.numeric(time(z)) %% 1, sumOrMeanFunction,na.rm = TRUE))
  }
  if(timeStep == "my"){
    return (aggregate(z, as.Date(as.yearmon(time(z))), sumOrMeanFunction,na.rm = TRUE))
  }
  if(timeStep == "y"){
    return (aggregate(z, format(as.Date(index(z)), '%y'), sumOrMeanFunction,na.rm = TRUE))
  }
  if(timeStep == "m"){
    return (aggregate(z, format(as.Date(index(z)), '%m'), sumOrMeanFunction,na.rm = TRUE))
  }
  if(timeStep == "sy"){
    return (aggregate(z, as.Date(as.yearqtr(time(z))), sumOrMeanFunction,na.rm = TRUE))
  }
  if(timeStep == "s"){
    return (aggregate(z, quarters(time(z)), sumOrMeanFunction,na.rm = TRUE))
  }
  
  print("Type not understood")
}



# ===================================
# ** From functions_post_treatment **
# ===================================


# -------------------------------Topologie--------------------------------------
# Topologie <- function (brin,reach)
#      find all of the reachable nodes from a given node in a graph
# Args:
#     brin: The index of the given HRU | brin pour lequel on veut la topologie 
#     reach: A matrix containing the indices of all HRUs and their downstream HRUs | le fichier parametre reach.par charge
# Returns:
#      A vector containing the indices of all HRUs upstream of the given HRU | la liste des brins en amont du brin choisi

# take in a vector of reachable nodes from a given node, and return a vector of all nodes that can be reached from the original node.
# Remontee depuis le brin choisi jusqu'a l'amont du bassin
# ------------------------------------------------------------------------------
Topologie <- function (brin, reach) { 
  IDs <- NULL
  Brin0 <- brin
  
  for (indice in 1:1000){
    assign(paste0('Brin', indice), NULL)
  }
  k <- 0
  
  while (length(get(paste0('Brin', k)))!=0){
    for (i in seq_along(get(paste0('Brin', k)))){
      assign(paste0('Brin', k + 1), c(get(paste0('Brin', k + 1)), reach[which(reach[, 2]== get(paste0('Brin', k))[i]), 1]))
    }
    k <- k+1
  }
  Total  <- brin
  for (l in 1:k){
    Total <- unique(c(Total,get(paste0('Brin', l))))
  }
  Total
}



# ==========================
# ** From MDR_AERMCprelev **
# ==========================


# -------------------------------Prelev82_1987_2007-----------------------------
# Prelev82_1987_2007 <- function()
#      Calculates the mean annual water withdrawal across all cantons over 1987-2007
# Args:
#     None
# Returns:
#      A dataframe containing the canton and the corresponding mean annual water withdrawal
# ------------------------------------------------------------------------------
Prelev82_1987_2007 <- function() { # m3/yr
  Prelev <- read.table('~/Documents/MDR/irrigation/Chronique_PrelevRMC_Cantons.txt',header=T)
  
  Prelev82 <- NULL
  for (cant in Prelev$Canton[order(unique(Prelev$Canton))]){
    Prelev82_ann <- Prelev[which(Prelev[, 1] == cant), 5] * 1000 #(m3)
    Prelev_date <- as.Date(as.character(Prelev[which(Prelev[, 1] == cant), 2]), format="%Y")
    Prelev82_ann <- xts(Prelev82_ann, Prelev_date)
    Prelev82 <- rbind(Prelev82, mean(Prelev82_ann["1987/2007"])) # prélèvement annuel moyen sur 1987-2007
  }
  Prelev82data <- cbind(Prelev$Canton[order(unique(Prelev$Canton))], Prelev82)
  colnames(Prelev82data) <- c('canton', 'Prelev82')
  
  return(Prelev82data)
}


# -------------------------------Prelev8182_1987_2007---------------------------
# Prelev8182_1987_2007 <- function()
#      Calculates the mean annual water withdrawals from 1981 to 1982 for each canton.
# Args:
#     None
# Returns:
#      A dataframe with the canton in the first column and the mean annual water withdrawals in the second column
# ------------------------------------------------------------------------------
Prelev8182_1987_2007 <- function(){ # m3/yr
  Prelev <- read.table('~/Documents/MDR/irrigation/Chronique_PrelevRMC_Cantons.txt',header=T)
  
  Prelev8182 <- NULL
  for (cant in Prelev$Canton[order(unique(Prelev$Canton))]){
    Prelev82_ann <- Prelev[which(Prelev[, 1] == cant), 5] * 1000 #(m3)
    Prelev81_ann <- Prelev[which(Prelev[, 1] == cant), 4] * 1000 #(m3)
    Prelev_date <- as.Date(as.character(Prelev[which(Prelev[, 1] == cant), 2]), format="%Y")
    
    Prelev82_ann <- xts(Prelev82_ann, Prelev_date)
    Prelev81_ann <- xts(Prelev81_ann, Prelev_date)
    
    Prelev8182 <- rbind(Prelev8182, mean(Prelev81_ann["1987/2007"])+mean(Prelev82_ann["1987/2007"])) # prélèvement annuel moyen sur 1987-2007
  }
  Prelev8182data <- cbind(Prelev$Canton[order(unique(Prelev$Canton))], Prelev8182)
  colnames(Prelev8182data) <- c('canton', 'Prelev8182')
  
  return(Prelev8182data)
}


# -------------------------------Prelev8182_2008_2012---------------------------
# Prelev8182_2008_2012 <- function()
#      Finds the average annual water withdrawals for all cantons from 2008-2012.
# Args:
#     None
# Returns:
#      A dataframe containing the canton and the corresponding average annual water withdrawals
# ------------------------------------------------------------------------------
# m3/yr 
Prelev8182_2008_2012 <- function(){
  Prelev <- read.table('~/Documents/MDR/irrigation/Chronique_PrelevRMC_Cantons.txt',header=T) # create a data.frame from the .txt file
  
  Prelev8182 <- NULL
  for (cant in Prelev$Canton[order(unique(Prelev$Canton))]){
    Prelev82_ann <- Prelev[which(Prelev[,1] == cant),5] * 1000 #(m3) create a vector with all the annual data of the first canal for the canton cant
    Prelev81_ann <- Prelev[which(Prelev[,1] == cant),4] * 1000 #(m3) same but with the second canal
    Prelev_date <- as.Date(as.character(Prelev[which(Prelev[,1] == cant),2]), format="%Y") # create a vector with the date of all the data
    
    Prelev82_ann <- xts(Prelev82_ann, Prelev_date) # create a time series with the data of the first canal and their date
    Prelev81_ann <- xts(Prelev81_ann, Prelev_date) # same with the second canal
    
    Prelev8182 <- rbind(Prelev8182,mean(Prelev81_ann["2008/2012"])+mean(Prelev82_ann["2008/2012"])) # add to a vector the mean of the time series between 2008 and 2012 (5 years)
  }
  Prelev8182data <- cbind(Prelev$Canton[order(unique(Prelev$Canton))], Prelev8182) # create a data.frame with the canton and the mean of 5 years
  colnames(Prelev8182data) <- c('canton','Prelev8182') # give a name to the columns
  
  return(Prelev8182data)
}

