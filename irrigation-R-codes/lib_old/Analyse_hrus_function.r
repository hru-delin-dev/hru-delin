Analyse_hrus <- function (folder_parameter,Catchment,Name_subCatch,watershed,rewrite){

############################################################################
#  folder_parameter : folder where the parameter files are
#  Catchment : Name of the catchment
#  Name_subCatch : if you have sub-catchment, name of one sub-catchment, else NULL
#  watershed : watershed corresponding to the sub-catchment if you have one, else NULL
#  rewrite : if TRUE, the code is creating a new file
############################################################################

#Lecture HRU.par
hrus <- read.table(paste(folder_parameter,'hrus.par',sep=''),col.names=read.table(paste(folder_parameter,'hrus.par',sep=''),skip=1,nr=1,colClasses="character"),skip=5)
#Lecture soils.par
soils <- read.table(paste(folder_parameter,'soils.par',sep=''),col.names=read.table(paste(folder_parameter,'soils.par',sep=''),skip=1,nr=1,colClasses="character"),skip=5,comment.char='#')
#Lecture hgeo.par
hgeo <- read.table(paste(folder_parameter,'hgeo.par',sep=''),col.names=read.table(paste(folder_parameter,'hgeo.par',sep=''),skip=1,nr=1,colClasses="character"),skip=5,comment.char='#')
#Lecture landuse.par
landuse <- read.table(paste(folder_parameter,'landuse.par',sep=''),col.names=read.table(paste(folder_parameter,'landuse.par',sep=''),skip=1,nr=1,colClasses="character"),skip=5,comment.char='#')


#Rewrite the file if necessary
if(rewrite){write.table(NULL,paste(folder_parameter,"Resume_",Catchment,".txt",sep=""),col.names=F,row.names=F,quote=F,sep='\t',append=F)}


if (length(Name_subCatch) != 0){
if (length(watershed) != 0){
#Extraction of the hrus of the sub-catchment
hrus2 <- NULL
NbWatershed <- length(watershed)
nb <- 0
while(nb != NbWatershed){
nb <- nb + 1
hrus2 <- rbind(hrus2,hrus[(hrus$watershed == watershed[nb]),])
}
}
} else {hrus2 <- hrus}

#Volume LPS (= aircap*Area/1000 en m3)
Volume_LPS <- NULL
for (i in c(1:dim(hrus2)[1])){
Volume_LPS <- c(Volume_LPS,soils$aircap[hrus2$soilID [i] == soils$SID]*as.numeric(hrus2$area[i])/1000)
}

#We remove the 'fc_sum' column
if(length(which(colnames(soils) == 'fc_sum'))==1){soils <- soils[,-which(colnames(soils) == 'fc_sum')]}
#Reorganisation of fc values
indice_fc <- which(substr(colnames(soils),1,2)=="fc")
fc_value <- as.numeric(substr(colnames(soils)[which(substr(colnames(soils),1,2)=="fc")],4,6))
fc_number <- cbind(fc_value,indice_fc)

#Volume MPS (= sum(fc_i for i<rootdepth)*Area/1000 in m3)
Volume_MPS <- NULL
for (i in c(1:dim(hrus2)[1])){
rootdepth <- round(as.numeric(landuse$rootDepth[hrus2$landuseID [i] == landuse$LID]),0)
Volume_MPS <- c(Volume_MPS,ifelse(rootdepth==0,0,sum(soils[hrus2$soilID [i] == soils$SID,fc_number[fc_number[,1] <= rootdepth,2]]) *as.numeric(hrus2$area[i])/1000))
}

#Volume RG1 (=maxRG1 * Area / 1000)
Volume_RG1 <- NULL
for (i in c(1:dim(hrus2)[1])){
Volume_RG1 <- c(Volume_RG1,hgeo$RG1_max[hrus2$hgeoID [i] == hgeo$GID] *as.numeric(hrus2$area[i])/1000)
}

#Interception (= LAI * alpha * Area / 1000) depending on the period
#For the model with snow, the value calculated by the code is slightly under-estimed
alpha = 1


Volume_IntD1 <- NULL
Volume_IntD2 <- NULL
Volume_IntD3 <- NULL
Volume_IntD4 <- NULL
for (i in c(1:dim(hrus2)[1])){
Volume_IntD1 <- c(Volume_IntD1,landuse$LAI_d1[hrus2$landuseID [i] == landuse$LID] * alpha  * as.numeric(hrus2$area[i])/1000)
Volume_IntD2 <- c(Volume_IntD2,landuse$LAI_d2[hrus2$landuseID [i] == landuse$LID] * alpha  *as.numeric(hrus2$area[i])/1000)
Volume_IntD3 <- c(Volume_IntD3,landuse$LAI_d3[hrus2$landuseID [i] == landuse$LID] * alpha  *as.numeric(hrus2$area[i])/1000)
Volume_IntD4 <- c(Volume_IntD4,landuse$LAI_d4[hrus$landuseID [i] == landuse$LID] * alpha  *as.numeric(hrus2$area[i])/1000)
}

#Total storage volume on the catchment for each period
Vtot_d1 <- sum(Volume_MPS + Volume_LPS + Volume_RG1 + Volume_IntD1)
Vtot_d2 <- sum(Volume_MPS + Volume_LPS + Volume_RG1 + Volume_IntD2)
Vtot_d3 <- sum(Volume_MPS + Volume_LPS + Volume_RG1 + Volume_IntD3)
Vtot_d4 <- sum(Volume_MPS + Volume_LPS + Volume_RG1 + Volume_IntD4)

#Contribution of LPS
PercLPS_d1 <- sum(Volume_LPS) / Vtot_d1 * 100
PercLPS_d2 <- sum(Volume_LPS) / Vtot_d2 * 100
PercLPS_d3 <- sum(Volume_LPS) / Vtot_d3 * 100
PercLPS_d4 <- sum(Volume_LPS) / Vtot_d4 * 100

#Contribution of MPS
PercMPS_d1 <- sum(Volume_MPS) / Vtot_d1 * 100
PercMPS_d2 <- sum(Volume_MPS) / Vtot_d2 * 100
PercMPS_d3 <- sum(Volume_MPS) / Vtot_d3 * 100
PercMPS_d4 <- sum(Volume_MPS) / Vtot_d4 * 100

#Contribution of RG1
PercRG1_d1 <- sum(Volume_RG1) / Vtot_d1 * 100
PercRG1_d2 <- sum(Volume_RG1) / Vtot_d2 * 100
PercRG1_d3 <- sum(Volume_RG1) / Vtot_d3 * 100 
PercRG1_d4 <- sum(Volume_RG1) / Vtot_d4 * 100      

#Contribution of Interception storage
PercIntc1_d1 <- sum(Volume_IntD1) / Vtot_d1 * 100
PercIntc1_d2 <- sum(Volume_IntD2) / Vtot_d2 * 100
PercIntc1_d3 <- sum(Volume_IntD3) / Vtot_d3 * 100
PercIntc1_d4 <- sum(Volume_IntD4) / Vtot_d4 * 100

#Write the name of the sub-catchment
write.table(rbind("---------------------------------------------------------------------",ifelse(length(Name_subCatch)!=0,Name_subCatch,Catchment),""),paste(folder_parameter,"Resume_",Catchment,".txt",sep=""),col.names=F,row.names=F,quote=F,sep='\t',append=T)

#Write the final table with all results
Resume <- cbind(c("d1","d2","d3","d4"),c(format(Vtot_d1,scientific=TRUE,digits=3),format(Vtot_d2,scientific=TRUE,digits=3),format(Vtot_d3,scientific=TRUE,digits=3),format(Vtot_d4,scientific=TRUE,digits=3)),rbind(round(c(PercLPS_d1,PercMPS_d1,PercRG1_d1,PercIntc1_d1),2),round(c(PercLPS_d2,PercMPS_d2,PercRG1_d2,PercIntc1_d2),2),round(c(PercLPS_d3,PercMPS_d3,PercRG1_d3,PercIntc1_d3),2),round(c(PercLPS_d4,PercMPS_d4,PercRG1_d4,PercIntc1_d4),2)))
colnames(Resume) <- c("Period","Vtot(m3)","%LPS","%MPS","%RG1","%intercept")
write.table(Resume,paste(folder_parameter,"Resume_",Catchment,".txt",sep=""),col.names=T,row.names=F,quote=F,sep='\t',append=T)

# Add the catchment area in the file
Area_Catchment <- sum(as.numeric(hrus2$area))
write.table(paste("Catchment area : ", round(Area_Catchment/1000000,0)," km2",sep=""),paste(folder_parameter,"Resume_",Catchment,".txt",sep=""),col.names=F,row.names=F,quote=F,sep='\t',append=T)
}