#~******************************************************************************
#~*Aggregate zoo object at different time step
#~******************************************************************************
#~* PROGRAMMER: Meriem Labbas, Irstea Lyon
#~******************************************************************************
#~* CREATED/MODIFIED: Created February 2014
#~******************************************************************************
#~* CONTENTS
#~*    1. aggregateZoo
#~******************************************************************************
#~* COMMENTS : ONLY FOR ZOO OBJECT WITH AN INDEX CREATED WITH THE CHRON FUNCTION
#~******************************************************************************

require(chron)
require(zoo)



aggregateZoo <- function (z, timeStep, sumOrMeanFunction){ # NTC
  #^******************************************************************************
  #^* IN
  #^*  	1. zoo object
  #^*		2. type of time step aggregation :
  #^*  	             - "dmy"/"d" for daily aggregation, // 'd' add by IG
  #^*                 -   "my" for mensual aggregation,
  #^*                 -   "m" for interannual mensual aggregation,
  #^*                 -   "sy" for seasonnal aggregation,
  #^*                 -   "s" for interannual seasonnal aggregation
  #^*		3. type of aggregation : "mean" or "sum"
  #^* OUT
  #^*		1. zoo object
  #^******************************************************************************
  
  if(timeStep == "h"){
    return (aggregate(z, function(x) trunc(x, "01:00:00"), sumOrMeanFunction,na.rm = TRUE))
  }
  
  if(timeStep == "dmy"){
    return (aggregate(z, time(z) - as.numeric(time(z)) %% 1, sumOrMeanFunction,na.rm = TRUE))
  }
  if(timeStep == "d"){
    return (aggregate(z, as.Date(as.POSIXct(time(z))), sumOrMeanFunction,na.rm = TRUE))
  }
  
  if(timeStep == "my"){
    return (aggregate(z, as.Date(as.yearmon(time(z))), sumOrMeanFunction,na.rm = TRUE))
  }
  
  if(timeStep == "y"){
    return (aggregate(z, format(as.Date(index(z)), '%Y'), sumOrMeanFunction,na.rm = TRUE))
  }
  
  if(timeStep == "m"){
    return (aggregate(z, format(as.Date(index(z)), '%m'), sumOrMeanFunction,na.rm = TRUE))
  }
  
  if(timeStep == "sy"){
    return (aggregate(z, as.Date(as.yearqtr(time(z))), sumOrMeanFunction,na.rm = TRUE))
  }
  
  if(timeStep == "s"){
    return (aggregate(z, quarters(time(z)), sumOrMeanFunction,na.rm = TRUE))
  }
  
  print("Type not understood")
  
}


