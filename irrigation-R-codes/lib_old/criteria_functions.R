#### Criteria
#### This file contains functions for calculating validation criteria on runoff for a hydrological model (fixed time step!!)

### Function Nash
### function that computes Nash criterion of a time series
## Inputs:
### sim = univariate zoo object containing simulatedRunoff (any fixed time step)
### obs = univariate zoo object containing obsRunoff (any fixed time step)
Nash <- function(sim, obs)
{
  # Calculate Nash
  Nash <- 1 - sum(((sim - obs))^2, na.rm = TRUE)/
    sum(((obs- mean(obs, na.rm = TRUE)))^2, na.rm = TRUE);

  Nash
}

### Function sqrtNash
### function that computes the sqrt Nash criterion of a time series
### sim = univariate zoo object containing simulatedRunoff (any time step, runoff in m3/s)
### obs = univariate zoo object containing obsRunoff (any time step, runoff in m3/s)
sqrtNash <- function(sim, obs)
{
  # Calculate Nash
  Nash <- 1 - sum(((sqrt(sim) - sqrt(obs))^2), na.rm = TRUE)/
    sum(((sqrt(obs)- mean(sqrt(obs),na.rm=TRUE))^2), na.rm = TRUE);

  Nash
}

### Function logNash
### function that computes the log Nash criterion of a time series (adds a small value to all Q values to avoid log(0) problems)
### sim = univariate zoo object containing simulatedRunoff (any time step, runoff in m3/s)
### obs = univariate zoo object containing obsRunoff (any time step, runoff in m3/s)
### value to add to all the discharges to avoid problems (typically meanannualflow / 40)
### be careful the value chosen might slightly change the value of the Nash criterion
logNash <- function(sim, obs, value)
{
  # Calculate Nash
  Nash <- 1 - sum(((log(sim+value) - log(obs+value))^2), na.rm = TRUE)/
    sum(((log(obs+value)- mean(log(obs+value),na.rm=TRUE))^2), na.rm = TRUE);

  Nash
}


### Function bias
### function that computes simulation bias in %
### sim = univariate zoo object containing simulatedRunoff (any time step, runoff in m3/s)
### obs = univariate zoo object containing obsRunoff (any time step, runoff in m3/s)
Bias <- function(sim,obs)
{
  # Calculate bias in %
  Bias <- sum((sim - obs), na.rm = TRUE)/
    sum(obs, na.rm = TRUE)*100;

Bias
}

### Function abias
### function that computes absolute simulation bias in %
### sim = univariate zoo object containing simulatedRunoff (any time step, runoff in m3/s)
### obs = univariate zoo object containing obsRunoff (any time step, runoff in m3/s)
aBias <- function(sim,obs)
{
  # Calculate bias in %
  Bias <- sum(abs(sim - obs), na.rm = TRUE)/
    sum(obs, na.rm = TRUE)*100;

Bias
}

### Function RMSE
### function that computes root mean square error
### sim = univariate zoo object containing simulatedRunoff (any time step, runoff in m3/s)
### obs = univariate zoo object containing obsRunoff (any time step, runoff in m3/s)
RMSE <- function(sim,obs)
{
  # Calculate variance
  var <- sum((sim - obs)^2, na.rm = TRUE)/
    length(sim);
  # RMSE is the square root of variance
  sqrt(var)
}

### Function corr
### function that computes???? I don't really know, I have to work on this a little more
### sim = univariate zoo object containing simulatedRunoff (any time step, runoff in m3/s)
### obs = univariate zoo object containing obsRunoff (any time step, runoff in m3/s)
corr <- function(sim,obs)
{
  # Calculate correlation
  cor(sim, obs, use="complete.obs");

}


### Function quantile
### function that gives quantiles 10 and 90 for sim and obs
### sim = univariate zoo object containing simulatedRunoff (any time step, runoff in m3/s)
quant <- function(sim)
{
  # Calculate quantiles
  quant_sim <- quantile(na.omit(sim),probs=c(0.1,0.9)) 
  return(quant_sim)
}


NashRel <- function(sim, obs)
{
  # Calculate Nash
  NashRel <- 1 - sum(((sim - obs)/(obs+0.01*mean(obs, na.rm = TRUE)))^2, na.rm = TRUE)/
    sum(((obs- mean(obs, na.rm = TRUE))/mean(obs+0.01*mean(obs, na.rm = TRUE), na.rm = TRUE))^2, na.rm = TRUE);
  NashRel
}

NashInv <- function(sim, obs)
{
  # Calculate Nash
  InvSim <- 1/(sim+0.01*mean(obs, na.rm = TRUE));
  InvObs <- 1/(obs+0.01*mean(obs, na.rm = TRUE));
  NashInv <- 1 - sum((InvSim - InvObs)^2, na.rm = TRUE)/sum((InvObs- mean(InvObs, na.rm = TRUE))^2, na.rm = TRUE);
  NashInv
}


### Function SetOfCriteria
### function that combines several functions above
### Nash, sqrtNash, logNash, bias, abias, RMSE
### Input
### sim = univariate zoo object containing simulatedRunoff (any time step, runoff in m3/s)
### obs = univariate zoo object containing obsRunoff (any time step, runoff in m3/s)
### value to add to all the discharges to avoid problems (typically meanannualflow / 40)
SetOfCriteria <- function(sim, obs, value)
{
  # Calculate Nash
  Nash <- Nash(sim,obs)
  NashInv <- NashInv(sim,obs)
  sqrtNash <- sqrtNash(sim,obs)
  logNash <- logNash(sim,obs,value)
  NashRel <- NashRel(sim,obs)
  RMSE <- RMSE(sim,obs)
  Bias <- Bias(sim,obs)
  aBias <- aBias(sim,obs)
  quant_sim <- quant(sim)
  quant_obs <- quant(obs)
  R10_R90 <- quant_sim/quant_obs
  QmeanSim <- mean(sim)
  QmeanObs <- mean(obs,na.rm=TRUE)  # return a vector combining all these results
  QmaxSim <- max(sim)
  QmaxObs <- max(obs,na.rm=TRUE)

  c(Nash,NashInv,sqrtNash,logNash,NashRel,RMSE , Bias, aBias,quant_sim,quant_obs,R10_R90,QmeanSim,QmeanObs,QmaxSim,QmaxObs)
}
