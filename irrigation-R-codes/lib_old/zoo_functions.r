library(chron)
library(zoo)

createZoo <- function (table, date_start, date_end,list_val,format_date)
{
  #Creer un objet zoo ? partir de la table [table]
  #sur la p?riode [date_start] ("yyyy-mm-dd" incluse) ? [date_end] (excluse) avec les variables
  #dont l'ent?te est dans la liste [list_val] pour un format de date choisi [format_date]
  
  #Trouver les indexs correspondant aux dates
  date <- as.Date(rownames(table), format_date)
  index_start <- which(date==date_start,arr.ind=TRUE)[1]
  index_end   <- which(date==date_end,arr.ind=TRUE)[1]
  
  #Mets les variables d'interet et sur la periode donn?e dans values. Idem pour date
  values <- table[index_start:index_end,list_val]
  date <- as.character(strptime(date[index_start:index_end],"%Y-%m-%d"))
  
  # Get the times and convert it to hh:mm:ss
  # if (length(table[index_start:index_end,"ID"])!=0) {heure <- as.character(table[index_start:index_end,"ID"])} else {
  heure <- rep(12,length(date))
  Times <- paste(heure,":00:00",sep="")
  
  # Create the chron dates-times series
  chron  <- chron (date,Times,format = c(dates = "y-m-d", times = "h:m:s"))
  
  #  Build the zoo vector of data
  values_zoo <- zoo(values,chron)
  
  return(values_zoo)
}

aggregateZoo <- function (z, timeStep, sumOrMeanFunction) # NTK ?? -- already in aggregateZoo_functions.r ><
{
  #Retourne un nouveau objet zoo aggr?g? sur le pas de temps timeStep
  #(["dmy","my","m","sy","s"]) en faisant la somme ou la moyenne ([sum,mean])
  
  if(timeStep == "dmy"){
    return (aggregate(z, time(z) - as.numeric(time(z)) %% 1, sumOrMeanFunction,na.rm = TRUE))
  }
  
  if(timeStep == "my"){
    return (aggregate(z, as.Date(as.yearmon(time(z))), sumOrMeanFunction,na.rm = TRUE))
  }
  
  if(timeStep == "y"){
    return (aggregate(z, format(as.Date(index(z)), '%y'), sumOrMeanFunction,na.rm = TRUE))
  }
  
  if(timeStep == "m"){
    return (aggregate(z, format(as.Date(index(z)), '%m'), sumOrMeanFunction,na.rm = TRUE))
  }
  
  if(timeStep == "sy"){
    return (aggregate(z, as.Date(as.yearqtr(time(z))), sumOrMeanFunction,na.rm = TRUE))
  }
  
  if(timeStep == "s"){
    return (aggregate(z, quarters(time(z)), sumOrMeanFunction,na.rm = TRUE))
  }
  
  print("Type not understood")
  
}


  
