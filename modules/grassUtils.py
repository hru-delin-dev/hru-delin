#!/usr/bin/env python3
# -*- coding: utf-8 -*-
############################################################################
#
# MODULE:       grassUtils.py
# AUTHOR(S):    Julien Veyssier
# 
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################




import os, sys, shutil
try:
    # Python 3
    from subprocess import DEVNULL
except ImportError:
    #DEVNULL = open('trace_step3', 'wb')
    DEVNULL = open(os.devnull, 'wb')
import grass.script as grass
from grass.script.utils import decode, encode
import subprocess
import platform
isWindows = (platform.system() == 'Windows')

################# command wrappers to automatically disable "show window" for dumb windows

if isWindows:
    def getSi():
        si = None
        if hasattr(subprocess, 'STARTUPINFO'):
            si = subprocess.STARTUPINFO()
            si.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        return si
else:
    def getSi():
        return None

def grass_run_command(*args, **kwargs):
    kwargs['startupinfo'] = getSi()
    return grass.run_command(*args, **kwargs)

def grass_parse_command(*args, **kwargs):
    kwargs['startupinfo'] = getSi()
    return grass.parse_command(*args, **kwargs)

def grass_feed_command(*args, **kwargs):
    kwargs['startupinfo'] = getSi()
    return grass.feed_command(*args, **kwargs)

def grass_read_command(*args, **kwargs):
    kwargs['startupinfo'] = getSi()
    return grass.read_command(*args, **kwargs)

def grass_pipe_command(*args, **kwargs):
    kwargs['startupinfo'] = getSi()
    return grass.pipe_command(*args, **kwargs)

################# environment utils

def exportRasters(rMap):
    for name in rMap:
        grass_run_command('r.out.gdal',
            input=name,
            output=rMap[name],
            overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

def exportRastersFromEnv(rMap, grassDbPath, location):
    os.environ['GISRC'] = os.path.join(grassDbPath, 'grassdata', location, '.grassrc')
    for name in rMap:
        grass_run_command('r.out.gdal',
            input='result',
            output=rMap[name],
            overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

def importRastersInEnv(rMap, grassDbPath, location):
    os.environ['GISRC'] = os.path.join(grassDbPath, 'grassdata', location, '.grassrc')
    for name in rMap:
        grass_run_command('r.in.gdal',
            flags='o',
            input=rMap[name],
            output=name,
            overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

def buildGrassEnv(grassDbPath, location):
    gisdbase = os.path.join(grassDbPath, 'grassdata')

    if os.path.exists(grassDbPath):
        shutil.rmtree(grassDbPath)
    os.mkdir(grassDbPath)
    os.mkdir(gisdbase)

    buildGrassLocation(grassDbPath, location)

def buildGrassLocation(grassDbPath, location):
    gisdbase = os.path.join(grassDbPath, 'grassdata')
    mapset = 'PERMANENT'

    if os.path.exists(os.path.join(gisdbase, location)):
        shutil.rmtree(os.path.join(gisdbase, location))
    os.mkdir(os.path.join(gisdbase, location))
    os.mkdir(os.path.join(gisdbase, location, mapset))

    content = '''proj:       1
zone:       0
north:      1
south:      0
east:       1
west:       0
cols:       1
rows:       1
e-w resol:  1
n-s resol:  1
top:        1
bottom:     0
cols3:      1
rows3:      1
depths:     1
e-w resol3: 1
n-s resol3: 1
t-b resol:  1
'''
    with open(os.path.join(gisdbase, location, mapset, 'WIND'), 'w') as w:
        w.write(content)
    with open(os.path.join(gisdbase, location, mapset, 'DEFAULT_WIND'), 'w') as dw:
        dw.write(content)

    rc_file = os.path.join(gisdbase, location, '.grassrc')
    with open(rc_file, 'w') as rc:
        rc.write('GISDBASE: %s\n' % (os.path.abspath(gisdbase)))
        rc.write('LOCATION_NAME: %s\n' % location)
        rc.write('MAPSET: %s\n' % mapset)



