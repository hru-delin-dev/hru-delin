#!/usr/bin/env python3
# -*- coding: utf-8 -*-


############################################################################
#
# MODULE:       hru-delin_basins.py
# AUTHOR(S):    adapted from GRASS-HRU (ILMS) - JENA University
#               by IRSTEA - Christine Barachet,
#               Julien Veyssier
#               Michael Rabotin
#               Florent Veillon
# PURPOSE:      1. Relocates the gauges on the reaches
#               2. Calculates watersheds at the gauges
#               
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################




# to keep python2 compatibility
from __future__ import print_function
import string, os, sys, glob, types, time, platform
import numpy as np
try:
    import ConfigParser
except Exception as e:
    import configparser as ConfigParser
#import grass.script as grass
from grass.script.utils import decode, encode
import struct, math, csv, shutil

from osgeo import gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
from osgeo import ogr

import multiprocessing
from multiprocessing import Pool, cpu_count

from utils import isint, write_log
from reach import snapping_points_to_reaches, cut_streams_at_points
from reach import updateAttributeTable, processReachStats

MY_ABS_PATH=os.path.abspath(__file__)
MY_DIR=os.path.dirname(MY_ABS_PATH)

try:
    # Python 3
    from subprocess import DEVNULL
except ImportError:
    DEVNULL = open(os.devnull, 'wb')

import pandas as pd
from rastertodataframe import raster_to_dataframe

'''

 MAIN

'''
def main(parms_file, nbProc, generator=False):
    
    """OUTPUT files
    - basins.tif
    """
    print(" ")
    print('---------- HRU-delin Step 2-2 started ---------------------------------------------')
    print("-----------------------------------------------------------------------------------")
    
    configFileDir = os.path.dirname(parms_file)
    parms = ConfigParser.ConfigParser(allow_no_value=True)
    tmpPath = os.path.join(configFileDir, 'tmp')
    if not os.path.isdir(tmpPath):
        os.mkdir(tmpPath)
    parms.read(parms_file)
    directory_out = parms.get('dir_out', 'files')
    # manage absolute and relative paths
    if not os.path.isabs(directory_out):
        directory_out = os.path.join(configFileDir, directory_out)
        
    #open list_point.csv
    file = pd.read_csv(os.path.join(directory_out,"list_point.csv"),header=None)
    #convert to tuples
    list_point = list(file.itertuples(index=False, name=None))
    nb_points = len(list_point)
    #Set Grass environnement
    os.environ['GISRC'] = os.path.join(configFileDir, 'grass_db', 'grassdata', 'hru-delin', '.grassrc')
    # Import drain raster
    drain_layer = os.path.join(directory_out, 'step1_drain.tif')
    drain_wk = 'drain_wk'
    grass_run_command('r.in.gdal', flags='o', input=drain_layer, output=drain_wk, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('g.proj', flags='p', georef=drain_layer, stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('g.region', flags='sp', raster=drain_wk, stdout=DEVNULL, stderr=DEVNULL)
    
    i = 0
    basins = 'basins'
    rasters_list = ['basins']
    max_rasters = 29

    
    
    print('---------- HRU-delin Step 2-2 : r.water.outlet')
    
    for points in list_point:
    
        grass_run_command('r.water.outlet',
            input=drain_wk, output='basin_tmp%d' % i,
            coordinates='%s,%s' % (points[0], points[1]),
            overwrite='True',
            stdout=DEVNULL, stderr=DEVNULL
        )
    

        rasters_list.append('basin_tmp%d' % i)
        i += 1
        
        if i % max_rasters == 0 or i == nb_points:
            grass_run_command('r.cross', input=','.join(rasters_list), output=basins, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
            grass_run_command('g.remove', flags='f', type='raster', pattern='basin_tmp*', stdout=DEVNULL, stderr=DEVNULL)
            rasters_list = ['basins']
    
    if generator:
        yield 5
    
    # with grass7 watershed ids begin at 0
    # empty areas value is already NULL, no need to set it
    grass_run_command('r.mapcalc', expression='basins=basins+1', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    #TODO remove this line
    grass_run_command('r.out.gdal', input='basins', output=os.path.join(directory_out, 'basins.tif'),
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    
    
    
    #TEST EXIST FILES AND FILL FILES
    print('---------- HRU-delin Step 2-2 : Test of existing and completed files')
    
    #basins.tif
    tif_path = os.path.join(directory_out, 'basins.tif')
    
    if os.stat(tif_path).st_size == 0:
        print('--------------- basins.tif is empty or nonexistent')
    else:
        tif_gdal = gdal.Open(tif_path)
        tif_band = tif_gdal.GetRasterBand(1)
        (min_tif,max_tif) = tif_band.ComputeRasterMinMax(True)

        if min_tif > 0 and max_tif <= nb_points:
            print('--------------- basins.tif is created and it has',nb_points, "watersheds")
        else :
            print("--------------- basins.tif is created but it empty")

    print('---------- HRU-delin Step 2-2 ended ---------------------------------------------')







if __name__ == '__main__':
    from grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from progressColors import *
    # check TQDM presence only if we are executed
    try:
        from tqdm import tqdm
    except Exception as e:
        print('!! %stqdm module not found%s\n' % (COLOR_RED, COLOR_RESET))
        sys.exit(1)

    parms_file = 'hrudelin_config.cfg'
    nbProcArg = ''
    if len(sys.argv) > 1:
        parms_file = sys.argv[1]
        if len(sys.argv) > 2:
            nbProcArg = sys.argv[2]

    # determine how many processes we can launch
    if str(nbProcArg).isnumeric() and int(nbProcArg) > 0:
        nbProc = int(nbProcArg)
    else:
        nbProc = cpu_count()

    # main is a generator but we don't use it here
    for pc in main(parms_file, nbProc, False):
        pass

    try:
        os.system('notify-send "hru-delin-6-2 step 2-2 complete"')
    except Exception as e:
        pass
else:
    from .grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from .progressColors import *
