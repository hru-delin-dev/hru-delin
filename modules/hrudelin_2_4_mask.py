#!/usr/bin/env python3
# -*- coding: utf-8 -*-


############################################################################
#
# MODULE:       hru-delin_basins.py
# AUTHOR(S):    adapted from GRASS-HRU (ILMS) - JENA University
#               by IRSTEA - Christine Barachet,
#               Julien Veyssier
#               Michael Rabotin
#               Florent Veillon
# PURPOSE:      1. Relocates the gauges on the reaches
#               2. Calculates watersheds at the gauges
#               
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################




# to keep python2 compatibility
from __future__ import print_function
import string, os, sys, glob, types, time, platform
import numpy as np
try:
    import ConfigParser
except Exception as e:
    import configparser as ConfigParser
#import grass.script as grass
from grass.script.utils import decode, encode
import struct, math, csv, shutil

from osgeo import gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
from osgeo import ogr

import multiprocessing
from multiprocessing import Pool, cpu_count

from utils import isint, write_log
from reach import snapping_points_to_reaches, cut_streams_at_points
from reach import updateAttributeTable, processReachStats

MY_ABS_PATH=os.path.abspath(__file__)
MY_DIR=os.path.dirname(MY_ABS_PATH)

try:
    # Python 3
    from subprocess import DEVNULL
except ImportError:
    DEVNULL = open(os.devnull, 'wb')

import pandas as pd
from rastertodataframe import raster_to_dataframe

'''

 MAIN

'''
def main(parms_file, nbProc, generator=False):
    
    """OUTPUT files
    - step2_mask.tif
    """
    print(" ")
    print('---------- HRU-delin Step 2-4 started ---------------------------------------------')
    print("-----------------------------------------------------------------------------------")
    
    configFileDir = os.path.dirname(parms_file)
    parms = ConfigParser.ConfigParser(allow_no_value=True)
    tmpPath = os.path.join(configFileDir, 'tmp')
    if not os.path.isdir(tmpPath):
        os.mkdir(tmpPath)
    parms.read(parms_file)
    directory_out = parms.get('dir_out', 'files')
    # manage absolute and relative paths
    if not os.path.isabs(directory_out):
        directory_out = os.path.join(configFileDir, directory_out)
        
    #Set Grass environnement
    os.environ['GISRC'] = os.path.join(configFileDir, 'grass_db', 'grassdata', 'hru-delin', '.grassrc')
    # Import drain raster
    drain_layer = os.path.join(directory_out, 'step1_drain.tif')
    drain_wk = 'drain_wk'
    grass_run_command('r.in.gdal', flags='o', input=drain_layer, output=drain_wk, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('g.proj', flags='p', georef=drain_layer, stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('g.region', flags='sp', raster=drain_wk, stdout=DEVNULL, stderr=DEVNULL)

    #open list_point.csv
    file = pd.read_csv(os.path.join(directory_out,"list_point.csv"),header=None)
    #convert to tuples
    list_point = list(file.itertuples(index=False, name=None))
    # Get parameters from configuration file
    parms = ConfigParser.ConfigParser(allow_no_value=True)
    tmpPath = os.path.join(configFileDir, 'tmp')
    if not os.path.isdir(tmpPath):
        os.mkdir(tmpPath)
    parms.read(parms_file)
    
    basins_rcl_tif = os.path.join(directory_out, 'step2_watersheds.tif')
    basins_rcl = "basins_rcl"
    
    grass_run_command('r.in.gdal', flags='o', input=basins_rcl_tif, output=basins_rcl, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    
    
    grass_run_command('r.null', map=basins_rcl, setnull=0, stdout=DEVNULL, stderr=DEVNULL)
    mask_tmp = 'mask_tmp'
    pReclass = grass_feed_command('r.reclass', input=basins_rcl, output=mask_tmp, overwrite='True', rules='-')
    pReclass.stdin.write(encode('0 thru 10000000 = 1\n'))
    pReclass.stdin.close()
    pReclass.wait()

    # TODO check that (was removed before)
    #grass_run_command('r.null', map=mask_tmp, null=0, setnull=1, stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.out.gdal', input=mask_tmp, type='UInt16', output=os.path.join(directory_out, 'step2_mask.tif'),
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    # exit if mask is too small and reloc is disabled
    inGd = gdal.Open(os.path.join(directory_out, 'step2_mask.tif'))
    inband1 = inGd.GetRasterBand(1)
    indata1 = BandReadAsArray(inband1)
    inNodata = inband1.GetNoDataValue()

    nbGoodValues = np.count_nonzero(indata1 != inNodata)
    xl = inGd.RasterXSize
    yl = inGd.RasterYSize
    # mask represents less than 0.1 % of area
    maskProportion = nbGoodValues / (xl * yl) * 100
    maskIsTooSmall = maskProportion < 0.1
    if parms.get('auto_relocation', 'to_do') != 'yes' and maskIsTooSmall:
        sys.exit('!!! Relocation is disabled and you are getting a very small mask. Please consider enabling gauge relocation.')
    
    # Cutting the streams at gauges
    listReachID=cut_streams_at_points(directory_out,list_point,'step1_streams.tif','step2_streams_new.tif')
    
    #update attribute table with ReachID
    gauges_col_name = parms.get('gauges', 'gauges_col_name')
    updateAttributeTable(directory_out,listReachID,'step2_gauges_for_watersheds.shp',gauges_col_name)
    
    if str(parms.get('dams', 'to_do')) == 'yes':
        dams_col_name = parms.get('dams', 'dams_col_name')
        updateAttributeTable(directory_out,listReachID,'step2_dams_for_watersheds.shp',dams_col_name)

    if generator:
        yield 20
    
    print('---------- HRU-delin Step 2-4 ended ---------------------------------------------')
    
    if generator:
        yield 15

    
    
        
        
        
if __name__ == '__main__':
    from grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from progressColors import *
    # check TQDM presence only if we are executed
    try:
        from tqdm import tqdm
    except Exception as e:
        print('!! %stqdm module not found%s\n' % (COLOR_RED, COLOR_RESET))
        sys.exit(1)

    parms_file = 'hrudelin_config.cfg'
    nbProcArg = ''
    if len(sys.argv) > 1:
        parms_file = sys.argv[1]
        if len(sys.argv) > 2:
            nbProcArg = sys.argv[2]

    # determine how many processes we can launch
    if str(nbProcArg).isnumeric() and int(nbProcArg) > 0:
        nbProc = int(nbProcArg)
    else:
        nbProc = cpu_count()

    # main is a generator but we don't use it here
    for pc in main(parms_file, nbProc, False):
        pass

    try:
        os.system('notify-send "hru-delin-6-2 step 2-4 complete"')
    except Exception as e:
        pass
else:
    from .grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from .progressColors import *
