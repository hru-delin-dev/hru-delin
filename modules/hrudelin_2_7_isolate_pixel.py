#!/usr/bin/env python3
# -*- coding: utf-8 -*-


############################################################################
#
# MODULE:       hru-delin_basins.py
# AUTHOR(S):    adapted from GRASS-HRU (ILMS) - JENA University
#               by IRSTEA - Christine Barachet,
#               Julien Veyssier
#               Michael Rabotin
#               Florent Veillon
# PURPOSE:      1. Relocates the gauges on the reaches
#               2. Calculates watersheds at the gauges
#               
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################




# to keep python2 compatibility
from __future__ import print_function
import string, os, sys, glob, types, time, platform
import numpy as np
try:
    import ConfigParser
except Exception as e:
    import configparser as ConfigParser
#import grass.script as grass
from grass.script.utils import decode, encode
import struct, math, csv, shutil

from osgeo import gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
from osgeo import ogr

import multiprocessing
from multiprocessing import Pool, cpu_count

from utils import isint, write_log
from reach import snapping_points_to_reaches, cut_streams_at_points
from reach import updateAttributeTable, processReachStats

MY_ABS_PATH=os.path.abspath(__file__)
MY_DIR=os.path.dirname(MY_ABS_PATH)

try:
    # Python 3
    from subprocess import DEVNULL
except ImportError:
    DEVNULL = open(os.devnull, 'wb')

import pandas as pd
#pd.options.mode.chained_assignment = None
import geopandas as gpd
from rastertodataframe import raster_to_dataframe
import rtree
import pygeos

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


'''

 MAIN

'''
def main(parms_file, nbProc, generator=False):
    
    """OUTPUT files

    """
    print(" ")
    print('---------- HRU-delin Step 2-7 started ---------------------------------------------')
    print("-----------------------------------------------------------------------------------")
    
    configFileDir = os.path.dirname(parms_file)
    parms = ConfigParser.ConfigParser(allow_no_value=True)
    tmpPath = os.path.join(configFileDir, 'tmp')
    if not os.path.isdir(tmpPath):
        os.mkdir(tmpPath)
    parms.read(parms_file)
    directory_out = parms.get('dir_out', 'files')
    # manage absolute and relative paths
    if not os.path.isabs(directory_out):
        directory_out = os.path.join(configFileDir, directory_out)
    #Set Grass environnement
    os.environ['GISRC'] = os.path.join(configFileDir, 'grass_db', 'grassdata', 'hru-delin', '.grassrc')

    # Import step2_subbasins_2.tif
    step2_subbasins_layer = os.path.join(directory_out, 'step2_subbasins_2.tif')
    step2_subbasins_wk = 'step2_subbasins'
    grass_run_command('r.in.gdal', flags='o', input=step2_subbasins_layer, output=step2_subbasins_wk, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('g.proj', flags='p', georef=step2_subbasins_layer, stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('g.region', flags='sp', raster=step2_subbasins_wk, stdout=DEVNULL, stderr=DEVNULL)

    #STEP 1 : Raster to vector 
    print('---------- Creating vector layers from raster layers ... ')
    grass_run_command('r.to.vect', flags='v', quiet=True, input='step2_subbasins', output='step2_subbasins_vector', type='area', overwrite='True')
    grass_run_command('v.out.ogr',quiet=True,input='step2_subbasins_vector', type='area', format='ESRI_Shapefile', output=os.path.join(directory_out, 'step2_subbasins_2_vector.shp'), overwrite='True')
    
    #STEP 2 : Search shape with same ID
    print('---------- Search shape with same ID ... ')
    subbasins_vector = gpd.read_file(os.path.join(directory_out, 'step2_subbasins_2_vector.shp'))
    subbasins_vector_sameID = subbasins_vector[subbasins_vector.groupby(['cat'])['geometry'].transform('nunique') > 1]
    print(subbasins_vector_sameID)
    #subbasins_vector_sameID['index1'] = subbasins_vector_sameID.index
    subbasins_vector_sameID = subbasins_vector_sameID.rename_axis('index1').reset_index()
    
    
    print(subbasins_vector_sameID)
    
    #STEP 3 : Area calculation
    print('---------- Area calculation ... ')
    #create new column with area of each shape
    subbasins_vector_sameID = subbasins_vector_sameID.assign(area=subbasins_vector_sameID.area)
    print(subbasins_vector_sameID)
    
    #STEP 4 : Identification of smaller layer for each ID
    print('---------- Identification of smaller layer for each ID ... ')
    #Extraction of ID and row line of layer with smaller area
    pixel_sameID = subbasins_vector_sameID.groupby('cat', as_index=False)['area'].idxmin()
    #Set new column with correspondance or not (True/False)
    subbasins_vector_sameID["single"] = subbasins_vector_sameID.index1.isin(pixel_sameID.area)
    #Subset df with isolate pixel 
    single_pixels = subbasins_vector_sameID.loc[subbasins_vector_sameID['single'] == True,:]
    #print(single_pixels)
    #Export single pixels to .shp
    single_pixels.to_file(os.path.join(directory_out,"step2_single_pixels.shp"))
    
    #STEP 5 : Make buffer around single pixels
    print('---------- Make buffer ... ')
    single_pixels_shp = gpd.read_file(os.path.join(directory_out, 'step2_single_pixels.shp'))
    buffer_pixels = single_pixels_shp.buffer(10)
    #Export buffer pixels to .shp
    buffer_pixels.to_file(os.path.join(directory_out,"step2_buffer_pixels.shp"))
    
    #STEP 6 : Intersection between buffer pixel and subbasins_vector
    print('---------- Make intersection ... ')
    #buffer pixel
    buffer_pixels_shp = gpd.read_file(os.path.join(directory_out, 'step2_buffer_pixels.shp'))
    #subbasin vector
    subbasins_vector_shp = subbasins_vector 
    intersection = gpd.overlay(subbasins_vector_shp, buffer_pixels_shp, how='intersection')
    #print(intersection)
    
    #STEP 7 : identification of single error pixel 
    print('---------- Make identification ... ')
    #group by FID (1 FID for part of buffer)
    test = intersection.groupby('FID')
    
    #set empty geopandasdataframe
    df_error_pixel = gpd.GeoDataFrame()
    #group by iteration
    for name, group in test:
        lenght_same_ID = len(group)
        lenght_unique = len(group['cat'].unique())
#        print(lenght_same_ID )
#        print(lenght_unique )
        #if the buffer intersect only one shape with the same id 
        if lenght_same_ID ==2 and lenght_unique == 1:
            group = group.assign(area=group.area)
            #print(group)
            group = group[group.area == group.area.max()]
            df_error_pixel = df_error_pixel.append(group)
            
    #df_error_pixel["ID_gauges"] = np.nan
    #print(df_error_pixel)
    
    print('---------- Isolate pixels ? ... ')
    if len(df_error_pixel) >=1:
        print('---------------- Yes : ')
        for index, row in df_error_pixel.iterrows():
            print("---------------- ID of subbassins with isolate pixel : ", row["cat"])
        df_error_pixel.to_file(os.path.join(directory_out,"step2_error_pixels_subbasins.shp"))
    else :
        print('---------------- No')
    
#    #STEP 8 : Research for the nearest gauges 
#    print('---------- Research for the nearest gauges  ... ')
#    gauges_selected_newID = gpd.read_file(os.path.join(directory_out, 'gauges_selected.shp'))
#    #Recherche du plus proche voisin 
#    #ATTENTION : formatage du fichier d'origine gauge : mettre ID
#    test = gpd.sjoin_nearest(df_error_pixel, gauges_selected_newID)
#    print(test.gid)

    
    print('---------- HRU-delin Step 2-7 ended ---------------------------------------------')
        
        
        
if __name__ == '__main__':
    from grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from progressColors import *
    # check TQDM presence only if we are executed
    try:
        from tqdm import tqdm
    except Exception as e:
        print('!! %stqdm module not found%s\n' % (COLOR_RED, COLOR_RESET))
        sys.exit(1)

    parms_file = 'hrudelin_config.cfg'
    nbProcArg = ''
    if len(sys.argv) > 1:
        parms_file = sys.argv[1]
        if len(sys.argv) > 2:
            nbProcArg = sys.argv[2]

    # determine how many processes we can launch
    if str(nbProcArg).isnumeric() and int(nbProcArg) > 0:
        nbProc = int(nbProcArg)
    else:
        nbProc = cpu_count()

    # main is a generator but we don't use it here
  #  for pc in main(parms_file, nbProc, False):
   #     pass

    try:
        os.system('notify-send "hru-delin-6-2 step 2-7 complete"')
    except Exception as e:
        pass
else:
    from .grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from .progressColors import *
