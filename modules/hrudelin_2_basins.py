#!/usr/bin/env python3
# -*- coding: utf-8 -*-


############################################################################
#
# MODULE:       hru-delin_basins.py
# AUTHOR(S):    adapted from GRASS-HRU (ILMS) - JENA University
#               by IRSTEA - Christine Barachet,
#               Julien Veyssier
# PURPOSE:      1. Relocates the gauges on the reaches
#               2. Calculates watersheds at the gauges
#               
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################




# to keep python2 compatibility
from __future__ import print_function
import string, os, sys, glob, types, time, platform
import numpy as np
try:
    import ConfigParser
except Exception as e:
    import configparser as ConfigParser
#import grass.script as grass
from grass.script.utils import decode, encode
import struct, math, csv, shutil

from osgeo import gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
from osgeo import ogr

import multiprocessing
from multiprocessing import Pool, cpu_count

from utils import isint, write_log
from reach import snapping_points_to_reaches, cut_streams_at_points
from reach import updateAttributeTable, processReachStats

MY_ABS_PATH=os.path.abspath(__file__)
MY_DIR=os.path.dirname(MY_ABS_PATH)

try:
    # Python 3
    from subprocess import DEVNULL
except ImportError:
    DEVNULL = open(os.devnull, 'wb')


'''

 MAIN

'''
def main(parms_file, nbProc, generator=False):
    print('---------- HRU-delin Step 2 started ---------------------------------------------')

    

    configFileDir = os.path.dirname(parms_file)
    # create main env
    buildGrassEnv(os.path.join(configFileDir, 'grass_db'), 'hru-delin')
    os.environ['GISRC'] = os.path.join(configFileDir, 'grass_db', 'grassdata', 'hru-delin', '.grassrc')
    # Get parameters from configuration file
    parms = ConfigParser.ConfigParser(allow_no_value=True)
    tmpPath = os.path.join(configFileDir, 'tmp')
    if not os.path.isdir(tmpPath):
        os.mkdir(tmpPath)
    parms.read(parms_file)
    directory_out = parms.get('dir_out', 'files')
    # manage absolute and relative paths
    if not os.path.isabs(directory_out):
        directory_out = os.path.join(configFileDir, directory_out)

    

    # test parameters from configuration file
    # if auto_relocation == yes, test int value for surface_tolerance_1 and distance_tolerance_1
    if (parms.get('auto_relocation', 'to_do')) == 'yes':

        if not isint(parms.get('auto_relocation', 'surface_tolerance_1')):
                sys.exit('------------> ERROR : Surface_tolerance_1 value not provided or is not integer' ) 
        if not isint(parms.get('auto_relocation', 'distance_tolerance_1')):
                sys.exit('------------> ERROR : Distance_tolerance_1 value not provided or is not integer' ) 


    ## test if basin min size is valid
        if not isint(parms.get('basin_min_size', 'size')):
                sys.exit('------------> ERROR : Basin min size value not provided or is not integer' ) 




    # Get the shape of gauges
    gauges_file = parms.get('gauges', 'relocated_gauges')
    if gauges_file == '':
        gauges_file = os.path.join(directory_out, 'gauges_selected.shp')
    else:
        if ogr.Open(gauges_file) is None:
            sys.exit('------------> ERROR : Relocated Gauges file not found')


    gauges_in = ogr.Open(gauges_file)
    gauges_lyr = gauges_in.GetLayer()

    # Set the new shape
    gauges_reloc_name = 'step2_gauges_for_watersheds'
    gauges_reloc_file = os.path.join(directory_out, gauges_reloc_name + '.shp')
    driver = ogr.GetDriverByName('ESRI Shapefile')
    if os.path.exists(gauges_reloc_file):
        driver.DeleteDataSource(gauges_reloc_file)
    gauges_reloc_shp = driver.CreateDataSource(gauges_reloc_file)
    gauges_reloc_lyr = gauges_reloc_shp.CopyLayer(gauges_lyr, gauges_reloc_name)
   
    # Relocation of the gauges
    if (parms.get('auto_relocation', 'to_do')) == 'yes':
        print('---------- HRU-delin Step 2 : Relocation of the gauges')
        
        gauges_area_col_name = parms.get('gauges', 'gauges_area_col_name')
        gauges_col_name = parms.get('gauges', 'gauges_col_name')
        snapping_points_to_reaches(parms, directory_out,gauges_col_name,gauges_area_col_name,gauges_reloc_lyr,'gauges_reloc.csv','gauges')

    gauges_reloc_shp.ExecuteSQL('REPACK ' + gauges_reloc_lyr.GetName())
    gauges_reloc_shp.Destroy()

    # relocation of dams if provided
    dams_reloc_file=0
    if str(parms.get('dams', 'to_do')) == 'yes':
         # Get the shape of dams
        dams_file = parms.get('dams', 'relocated_dams')
        if dams_file == '':
            dams_file = os.path.join(directory_out, 'dams_selected.shp')
        else:
            if ogr.Open(dams_file) is None:
                sys.exit('------------> ERROR : Relocated dams file not found')


        dams_in = ogr.Open(dams_file)
        dams_lyr = dams_in.GetLayer()

        # Set the new shape
        dams_reloc_name = 'step2_dams_for_watersheds'
        dams_reloc_file = os.path.join(directory_out, dams_reloc_name + '.shp')
        driver = ogr.GetDriverByName('ESRI Shapefile')
        if os.path.exists(dams_reloc_file):
            driver.DeleteDataSource(dams_reloc_file)
        dams_reloc_shp = driver.CreateDataSource(dams_reloc_file)
        dams_reloc_lyr = dams_reloc_shp.CopyLayer(dams_lyr, dams_reloc_name)
   
        # Relocation of the dams
        if (parms.get('auto_relocation', 'to_do')) == 'yes':
            print('---------- HRU-delin Step 2 : Relocation of the dams')
            
            dams_area_col_name = parms.get('dams', 'dams_area_col_name')
            dams_col_name = parms.get('dams', 'dams_col_name')
            snapping_points_to_reaches(parms, directory_out,dams_col_name,dams_area_col_name,dams_reloc_lyr,'dams_reloc.csv','dams')

        dams_reloc_shp.ExecuteSQL('REPACK ' + dams_reloc_lyr.GetName())
        dams_reloc_shp.Destroy()




    # Import drain raster
    drain_layer = os.path.join(directory_out, 'step1_drain.tif')
    drain_wk = 'drain_wk'
    grass_run_command('r.in.gdal', flags='o', input=drain_layer, output=drain_wk, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('g.proj', flags='p', georef=drain_layer, stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('g.region', flags='sp', raster=drain_wk, stdout=DEVNULL, stderr=DEVNULL)

    # Watersheds derivation
    basins = 'basins'
    rasters_list = ['basins']
        
    # r.cross doesn't accept more than 30 layers
    max_rasters = 29
    i = 0
    grass_run_command('r.mapcalc', expression='basins=null()', overwrite=True, stdout=DEVNULL, stderr=DEVNULL)
    #gauges
    gaugesDs = ogr.Open(gauges_reloc_file)
    gauges_reloc_lyr = gaugesDs.GetLayer()
    nb_gauges = gauges_reloc_lyr.GetFeatureCount()
    #dams
    nb_dams=0


    list_point=[]
    if str(parms.get('dams', 'to_do')) == 'yes':
        dams_col_name = parms.get('dams', 'dams_col_name')
        damsDs = ogr.Open(dams_reloc_file)
        dams_reloc_lyr = damsDs.GetLayer()
        nb_dams = dams_reloc_lyr.GetFeatureCount()
        
        for dam in dams_reloc_lyr:
            geom = dam.GetGeometryRef()
            dam_x, dam_y = geom.GetX(), geom.GetY()
            dam_ID=int(dam.GetField(dams_col_name))
            tuple_dam=(dam_x,dam_y,dam_ID)
            list_point.append(tuple_dam) 

    nb_points=nb_gauges+nb_dams
    #test if nb_points still has feature
    if nb_points == 0:
        print("Error on gauges (and dams if provided) relocated layer : 0 features found ") 
        print("Maybe check basin min size parameter") 
        sys.exit()
    
    gauges_col_name = parms.get('gauges', 'gauges_col_name')
    for gauge in gauges_reloc_lyr:
        geom = gauge.GetGeometryRef()
        gauge_x, gauge_y = geom.GetX(), geom.GetY()
        gauge_ID=int(gauge.GetField(gauges_col_name))
        tuple_gauge=(gauge_x,gauge_y,gauge_ID)
        list_point.append(tuple_gauge)


       
    

    print('---------- HRU-delin Step 2 : Derivating watersheds at gauges (and dams if provided)')
  
    for points in list_point:

        grass_run_command('r.water.outlet',
            input=drain_wk, output='basin_tmp%d' % i,
            coordinates='%s,%s' % (points[0], points[1]),
            overwrite='True',
            stdout=DEVNULL, stderr=DEVNULL
        )
        rasters_list.append('basin_tmp%d' % i)
        i += 1
        
        if i % max_rasters == 0 or i == nb_points:
            grass_run_command('r.cross', input=','.join(rasters_list), output=basins, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
            grass_run_command('g.remove', flags='f', type='raster', pattern='basin_tmp*', stdout=DEVNULL, stderr=DEVNULL)
            rasters_list = ['basins']
            
   
        


    if generator:
        yield 5

    # with grass7 watershed ids begin at 0
    # empty areas value is already NULL, no need to set it
    grass_run_command('r.mapcalc', expression='basins=basins+1', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    #TODO remove this line
    grass_run_command('r.out.gdal', input='basins', output=os.path.join(directory_out, 'basins.tif'),
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)


    print('---------- HRU-delin Step 2 : Reclassifying the watersheds')
    ################
    basins_rcl = 'basins_rcl'

    pRecode = grass_feed_command('r.recode', input=basins, output=basins_rcl, overwrite='True', rules='-', quiet=True)

    
    for points in list_point:
        out = decode(grass_read_command('r.what', map=basins, coordinates='%s,%s' % (points[0], points[1])))
        cat_grass = out.rstrip(os.linesep).split('|')[3].strip()
        pRecode.stdin.write(encode('%s:%s:%s\n' % (cat_grass, cat_grass, points[2])))
    pRecode.stdin.close()
    pRecode.wait()

    grass_run_command('r.out.gdal',
        input=basins_rcl,
        type='UInt16',
        output=os.path.join(directory_out, 'step2_watersheds.tif'),
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    if generator:
        yield 10

    # Create mask = surface of all watersheds
    print('---------- HRU-delin Step 2 : Creating the mask')
    grass_run_command('r.null', map=basins_rcl, setnull=0, stdout=DEVNULL, stderr=DEVNULL)
    mask_tmp = 'mask_tmp'
    pReclass = grass_feed_command('r.reclass', input=basins_rcl, output=mask_tmp, overwrite='True', rules='-')
    pReclass.stdin.write(encode('0 thru 10000000 = 1\n'))
    pReclass.stdin.close()
    pReclass.wait()

    # TODO check that (was removed before)
    #grass_run_command('r.null', map=mask_tmp, null=0, setnull=1, stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.out.gdal', input=mask_tmp, type='UInt16', output=os.path.join(directory_out, 'step2_mask.tif'),
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    # exit if mask is too small and reloc is disabled
    inGd = gdal.Open(os.path.join(directory_out, 'step2_mask.tif'))
    inband1 = inGd.GetRasterBand(1)
    indata1 = BandReadAsArray(inband1)
    inNodata = inband1.GetNoDataValue()

    nbGoodValues = np.count_nonzero(indata1 != inNodata)
    xl = inGd.RasterXSize
    yl = inGd.RasterYSize
    # mask represents less than 0.1 % of area
    maskProportion = nbGoodValues / (xl * yl) * 100
    maskIsTooSmall = maskProportion < 0.1
    if parms.get('auto_relocation', 'to_do') != 'yes' and maskIsTooSmall:
        sys.exit('!!! Relocation is disabled and you are getting a very small mask. Please consider enabling gauge relocation.')

    if generator:
        yield 15

    # Cutting the streams at gauges
    print('---------- HRU-delin Step 2 : Cutting the streams at gauges and dams if provided')
    listReachID=cut_streams_at_points(directory_out,list_point,'step1_streams.tif','step2_streams_new.tif')
    

    #update attribute table with ReachID
    updateAttributeTable(directory_out,listReachID,'step2_gauges_for_watersheds.shp',gauges_col_name)

    if str(parms.get('dams', 'to_do')) == 'yes':
        updateAttributeTable(directory_out,listReachID,'step2_dams_for_watersheds.shp',dams_col_name)
        

    

  


    # This last bit of code was added by I. Horner.
    # > A new subassin layer is computed by the cross over of subassin and watershed.
    # > The resulting features are attributed the same ids as the reaches
    # > Everything is saved as raster AND shapefile after running r.to.vect, for easy plotting.
    print('---------- HRU-delin Step 2 : Computing subbasinXwatershed raster layer')

    dem_recl = os.path.join(directory_out, 'step1_dem_reclass.tif')
    grass_run_command('g.proj', flags='c', georef=dem_recl, stdout=DEVNULL, stderr=DEVNULL)

    print('---------- Importing raster \'step1_subbasins.tif\'')
    subbasins = os.path.join(directory_out, 'step1_subbasins.tif')
    grass_run_command('r.in.gdal', flags='o', input=subbasins, output='subbasins', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    print('---------- Importing raster \'step2_watersheds.tif\'')
    basins = os.path.join(directory_out, 'step2_watersheds.tif')
    grass_run_command('r.in.gdal', flags='o', input=basins, output='watersheds', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    print('---------- Importing raster \'step2_streams_new.tif\'')
    reachraster = os.path.join(directory_out, 'step2_streams_new.tif')
    grass_run_command('r.in.gdal', flags='o', input=reachraster, output='reachraster', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    print('---------- Compute cross product \'watersheds*100000+subbasins\'')
    grass_run_command('r.mapcalc', expression='cross1=watersheds*100000+subbasins', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    print('---------- Compute cross product cross1*reachraster')
    grass_run_command('r.mapcalc', expression='cross2=if(reachraster!=0, cross1, 0)', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    if generator:
        yield 20

    print('---------- Setting nulls in \'cross1*reachraster\' and \'reachraster\'')
    grass_run_command('r.null', map='cross2', setnull=0, stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.null', map='reachraster', setnull=0, stdout=DEVNULL, stderr=DEVNULL)

    print('---------- Saving \'step2_subbasins.tif\' (watersheds*100000+subbasins)')
    grass_run_command('r.out.gdal', input='cross1', output=os.path.join(directory_out, 'step2_subbasins.tif'),
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    print('---------- Computing links between (watersheds*100000+subbasins) and reach ids')
    reach_ids = decode(grass_read_command('r.stats', quiet=True, flags='nN', input='reachraster')).rstrip(os.linesep).split(os.linesep)
    reach_ids_cleaned = []
    subbasins_cleaned = []
    n_reach = len(reach_ids)
    print('')
    # main loop

    # export rasters that are necessary for parallel environments
    rastersForWorkers = {
        'reachraster': os.path.join(tmpPath, 'step2_reachraster.tif'),
        'cross1': os.path.join(tmpPath, 'step2_cross1.tif'),
    }
    exportRasters(rastersForWorkers)

    # save main grass env which is being overriden later
    MAIN_GISRC = os.environ['GISRC']

    # build the environments and load exported rasters in each of them
    grassDbPath = os.path.join(configFileDir, 'grass_db')
    for i in range(nbProc):
        location = 'hru-delin_%s' % (i+1)
        buildGrassLocation(grassDbPath, location)
        # set projection
        # TODO test with a raster we want to pass to //
        os.environ['GISRC'] = os.path.join(grassDbPath, 'grassdata', location, '.grassrc')
        dem_recl = os.path.join(directory_out, 'step1_dem_reclass.tif')
        grass_run_command('g.proj', flags='c', georef=dem_recl, stdout=DEVNULL, stderr=DEVNULL)

        importRastersInEnv(rastersForWorkers, grassDbPath, location)

    nbReachs = len(reach_ids)
    if generator:
        print('Starting reach loop with %s process' % nbProc)
        with Pool(nbProc) as p:
            params = [(id, configFileDir, nbProc) for (i, id) in enumerate(reach_ids)]
            results = []
            for i, _ in enumerate(p.imap_unordered(processReachStats, params), 1):
                results.append(_)
                loopProgress = i/nbReachs*100
                globalProgress = 25 + (loopProgress/100*60)
                yield globalProgress
    else:
        # this is the interesting part, launching N processes in parallel to process basins
        # the locks are here to prevent concurrent terminal tqdm writing
        with Pool(nbProc, initializer=tqdm.set_lock, initargs=(tqdm.get_lock(),)) as p:
            params = [(id, configFileDir, nbProc) for (i, id) in enumerate(reach_ids)]
            results = list(tqdm(p.imap_unordered(processReachStats, params),
                desc='[main process] get reach id => subbasins id [%s process] ' % nbProc,
                total=nbReachs,
                unit='reach',
                bar_format=bar_format1
            ))

    # merge results
    for r in results:
        reach_ids_cleaned.append(r[0])
        subbasins_cleaned.append(r[1])

    # restore main grass env
    os.environ['GISRC'] = MAIN_GISRC

    print('')
    grass_run_command('g.remove', flags='f', type='raster', name='MASK', stdout=DEVNULL, stderr=DEVNULL)

    print('---------- Reclassifying (watersheds*100000+subbasins)')
    pReclass = grass_feed_command('r.reclass', input='cross1', output='cross1_reclassed', rules='-', overwrite='True')
    for k in range(0, len(reach_ids_cleaned)):
        pReclass.stdin.write(encode('%s=%s\n' % (subbasins_cleaned[k], reach_ids_cleaned[k])))
    pReclass.stdin.close()
    pReclass.wait()
 

    if generator:
        yield 90

    print('---------- Saving \'step2_subbasins_2.tif\'')
    try:
        grass_run_command('r.out.gdal', input='cross1_reclassed', output=os.path.join(directory_out, 'step2_subbasins_2.tif'),
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    except:
        sys.exit('------------> ERROR : Too many reclass category; check the value of ID of gauges and/or dams')       

    print('---------- Creating vector layers from raster layers ... ')
    grass_run_command('r.to.vect', flags='v', quiet=True, input='subbasins', output='subbasins_vector', type='area', overwrite='True')
    grass_run_command('r.to.vect', flags='v', quiet=True, input='watersheds', output='watersheds_vector', type='area', overwrite='True')
    ## we could clean the vector data in case isolated pixels in edges produce false small areas
    ## (with no category because it comes from an isolated nodata pixel in watershed.tif)
    #rasterWsheds = gdal.Open(basins)
    #ulx, xres, xskew, uly, yskew, yres = rasterWsheds.GetGeoTransform()
    #pixelArea = abs(xres) * abs(yres)
    #grass_run_command('v.clean',
    #    #flags='v',
    #    quiet=True,
    #    input='watersheds_vector',
    #    type='area',
    #    tool='rmarea',
    #    threshold=pixelArea,
    #    output='watersheds_vector_clean',
    #    overwrite='True')
    grass_run_command('r.to.vect', flags='v', quiet=True, input='cross1', output='cross1_vector', type='area', overwrite='True')
    grass_run_command('r.to.vect', flags='v', quiet=True, input='cross1_reclassed', output='cross1_reclassed_vector', type='area', overwrite='True')

    if generator:
        yield 95

    # to make sure there is a projection in exported files (grass78 complains)
    grass_run_command('g.proj', flags='c', georef=drain_layer, stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('v.out.ogr',
        #flags='c',
        quiet=True,
        input='subbasins_vector', type='area', format='ESRI_Shapefile', output=os.path.join(directory_out, 'step2_step1_subbasins.shp'))
    # we avoid c flag to skip areas with no category (result of vectorisation error)
    grass_run_command('v.out.ogr',
        #flags='c',
        quiet=True,
        input='watersheds_vector', type='area', format='ESRI_Shapefile', output=os.path.join(directory_out, 'step2_step2_watersheds.shp'))
    grass_run_command('v.out.ogr',
        #flags='c',
        quiet=True,
        input='cross1_vector', type='area', format='ESRI_Shapefile', output=os.path.join(directory_out, 'step2_step2_subbasins.shp'))
    grass_run_command('v.out.ogr',
        #flags='c',
        quiet=True,
        input='cross1_reclassed_vector', type='area', format='ESRI_Shapefile', output=os.path.join(directory_out, 'step2_step2_subbasins_2.shp'))

    print('---------- HRU-delin Step 2 ended    ---------------------------------------------')

if __name__ == '__main__':
    from grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from progressColors import *
    # check TQDM presence only if we are executed
    try:
        from tqdm import tqdm
    except Exception as e:
        print('!! %stqdm module not found%s\n' % (COLOR_RED, COLOR_RESET))
        sys.exit(1)

    parms_file = 'hrudelin_config.cfg'
    nbProcArg = ''
    if len(sys.argv) > 1:
        parms_file = sys.argv[1]
        if len(sys.argv) > 2:
            nbProcArg = sys.argv[2]

    # determine how many processes we can launch
    if str(nbProcArg).isnumeric() and int(nbProcArg) > 0:
        nbProc = int(nbProcArg)
    else:
        nbProc = cpu_count()

    # main is a generator but we don't use it here
    for pc in main(parms_file, nbProc, False):
        pass

    try:
        os.system('notify-send "hru-delin step 2 complete"')
    except Exception as e:
        pass
else:
    from .grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_read_command, grass_pipe_command
    from .progressColors import *
