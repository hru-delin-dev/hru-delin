#!/usr/bin/env python3
# -*- coding: utf-8 -*-

############################################################################
#
# MODULE:       hrudelin_3_hrugen.py
# AUTHOR(S):    adapted from GRASS-HRU (ILMS) - JENA University
#               by IRSTEA - Christine Barachet,
#               Julien Veyssier
#               Michael Rabotin
#               Florent Veillon
#
# PURPOSE:      overlay of all selected layers
#               construction of HRUs
#               
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################





# to keep python2 compatibility
from __future__ import print_function
import string, os, sys, glob, types, time, shutil
import math
try:
    import ConfigParser
except Exception as e:
    import configparser as ConfigParser
#import grass.script as grass
from grass.script.utils import decode, encode

from osgeo import gdal
from osgeo import ogr

from io import StringIO
import pandas as pd

import multiprocessing
from multiprocessing import Pool, cpu_count






def isint(value):
    try:
        int(value)
        return True
    except ValueError:
        return False


def reclass(map_in, map_out, size):
    p = grass_pipe_command('r.stats', flags='lnNc', input=map_in)
      
    pReclass = grass_feed_command('r.reclass', overwrite=True, input=map_in, output=map_out, rules='-')
    for l in p.stdout:
        lSpl = decode(l).rstrip(os.linesep).split()
        if int(lSpl[1]) <= int(size):
            pReclass.stdin.write(encode('%s = %s\n' % (lSpl[0], lSpl[0])))
    p.wait()
    pReclass.stdin.close()
    pReclass.wait()



def count_only(map_in, size):
    statLines = decode(grass_read_command('r.stats', quiet=True, flags='lnNc', input=map_in)).rstrip(os.linesep).split(os.linesep)
    c = 0
   
    for l in statLines:
        lSpl = l.split()
        if int(lSpl[1]) <= int(size):
            c += 1
    return c
    #return int(os.popen('r.stats --quiet -lnNc input=%s | awk %s' % (map_in, AWK_cmd_2(size))).readlines()[0])

def getAreasUpTo(map_in, size):
    areas = []
    statLists = []
    statLines = decode(grass_read_command('r.stats', quiet=True, flags='lnNc', input=map_in)).rstrip(os.linesep).split(os.linesep)
    for l in statLines:
        statLists.append(l.strip().split())
    # sort on second column
    statLists = sorted(statLists, key=lambda x: int(x[1]))
    # awk
    intSize = int(size)
    old = 0
    for sl in statLists:
        intSl1 = int(sl[1])
        if old != intSl1 and intSl1 <= intSize:
            areas.append(intSl1)
            old = intSl1

    return areas


try:
    # Python 3
    from subprocess import DEVNULL
except ImportError:
    DEVNULL = open(os.devnull, 'wb')

MY_ABS_PATH=os.path.abspath(__file__)
MY_DIR=os.path.dirname(MY_ABS_PATH)



# function launched in parallel, yeyeah
def processSubbasin(params):
    
    id, configFileDir, iRun, nbBasins, min_area, buffer_distance, tmpPath, generator, nbProc = params
    # define which grass env we are using
    current = multiprocessing.current_process()
    processN = int(current._identity[0])
    # because multiprocess does not reset workers id numbers and we still use [1, N]
    processN = (processN % nbProc) + 1
    location = 'hru-delin_%s' % (processN)
    os.environ['GISRC'] = os.path.join(configFileDir, 'grass_db', 'grassdata', location, '.grassrc')
    
    grass_run_command('r.mask', raster='subbasins_msk', maskcats=id.rstrip('\n'), overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('g.region', raster='clumps', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('g.region', zoom='MASK', stdout=DEVNULL, stderr=DEVNULL)
    
    # Get size of subbasin
    subbasin_size = int(decode(grass_read_command('r.stats', quiet=True, flags='nNc', input='MASK')).rstrip(os.linesep).split(os.linesep)[0].split()[1])
    
    if (subbasin_size <= min_area):
    
        if not generator:
            iterable3 = tqdm(
                total=2,
                initial=0,
                #desc='',
                unit='phase',
                bar_format=bar_format_yellow_2,
                position=processN,
                leave=True
            )
            iterable3.set_description('[process %s] basin [%s/%s] id %s small basin %s' %
                (processN, padLeft(str(iRun), len(str(nbBasins))), nbBasins, pad(id.strip(), 6), pad('', 3)))
            iterable3.update(1)
        grass_run_command('r.statistics', base='MASK', cover='clumps', method='max', output='hruid',
                            overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
        hruid = int(decode(grass_read_command('r.stats', quiet=True, flags='lnNc', input='hruid')).rstrip(os.linesep).split(os.linesep)[0].split()[1])
        grass_run_command('r.mapcalc',
            expression='newmap3=if(MASK,%d,null())'%hruid,
            overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
        if not generator:
            iterable3.update(1)
            iterable3.close()
    else:

        # LOOP THAT ELIMINATES PIXELS ONLY
        sizes = range(2, min_area + 1)
        
        if generator:
            iterable2 = sizes
        else:
            iterable2 = tqdm(
                sizes,
                desc='[process %s] basin [%s/%s] id %s single pixels %s' %
                    (processN, padLeft(str(iRun), len(str(nbBasins))), nbBasins, pad(id.strip(), 6), pad('', 3)),
                unit='px',
                bar_format=bar_format_yellow_2,
                position=processN,
                leave=True
            )
            iterable2.set_description('[process %s] basin [%s/%s] id %s single pixels %s' %
                (processN, padLeft(str(iRun), len(str(nbBasins))), nbBasins, pad(id.strip(), 6), pad('', 1)))
        
        initmap = 'clumps'
        counter_old = 0
        
        while (True):
            
            counter = count_only(initmap, 1)
            
            if counter != counter_old :
                reclass(initmap, 'clumps_sz1', 1)
                               
                counter_old = counter
                grass_run_command('r.mapcalc', expression='base=clumps_sz1', overwrite='True',stdout=DEVNULL, stderr=DEVNULL)
                
                # MERGE HORIZONTAL/VERTICAL ADJACENT PIXELS
                grass_run_command('r.mapcalc',
                    expression='map_a=if((not(isnull(base))&&not(isnull(base[0,1]))),base[0,1],if((not(isnull(base))&&not(isnull(base[1,0]))),base[1,0],base))',
                    overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                grass_run_command('r.patch',
                    input='map_a,clumps_cp', output='newmap', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                grass_run_command('r.mapcalc', expression='clumps_cp=newmap', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                initmap = 'newmap'

                
            else:
                
                grass_run_command('g.copy', raster='clumps,clumps_sz1')
                grass_run_command('r.mapcalc', expression='newmap=clumps_cp', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                break
        # START TO ELIMINATE REMAINING AND ISOLATED PIXELS
        grass_run_command('r.mapcalc', expression='newmap_sz1=clumps_sz1', overwrite='True',stdout=DEVNULL, stderr=DEVNULL)
        grass_run_command('r.buffer',
            input='newmap_sz1', output='buffer', distances='%d'%buffer_distance,
            overwrite='True',stdout=DEVNULL, stderr=DEVNULL)
        
        grass_run_command('r.mapcalc', expression='newmap2=if((buffer==2),newmap,null())', overwrite='True',stdout=DEVNULL, stderr=DEVNULL)
        
        # there were sort/awk system calls here
        # now less filesystem access, cleaner code, most importantely: portable to windows
        # pandatana !
        stats1 = decode(grass_read_command('r.stats', quiet=True, flags='cnN', input='newmap')).rstrip(os.linesep)
        
        stats2 = decode(grass_read_command('r.stats', quiet=True, flags='nN', input='newmap2')).rstrip(os.linesep)
        
        stio1 = StringIO(stats1)
        df1 = pd.read_csv(stio1, header=None, dtype={0: int, 1: int}, sep=' ')
        stio2 = StringIO(stats2)
       
        if stats2:
            df2 = pd.read_csv(stio2, header=None, dtype={0: int}, sep=' ')
            df3 = df1.merge(df2, on=0, sort=True)
        else:
            df3=df1
        
      
            
        #out3Path = os.path.join(tmpPath, 'out3_proc%s' % processN)
        pReclass = grass_feed_command('r.reclass', input='newmap', output='test', rules='-', overwrite='True')
        for index, row in df3.iterrows():
            pReclass.stdin.write(encode('%s = %s %s\n' % (row[0], row[0], row[1])))
        pReclass.stdin.close()
        pReclass.wait()
        
        #os.system('cat %s/out3_proc%s | r.reclass --o input=newmap output=test rules=-' % (tmpPath, processN))
        grass_run_command('g.region', raster='clumps', stdout=DEVNULL, stderr=DEVNULL)
        grass_run_command('r.buffer', input='MASK', output='buf_mask', distances='%d'%buffer_distance,
                            overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
        grass_run_command('g.remove', flags='f', type='raster', name='MASK', stdout=DEVNULL, stderr=DEVNULL)
        grass_run_command('g.region', zoom='buf_mask', stdout=DEVNULL, stderr=DEVNULL)
        
        #os.system('echo "2 = 2 9999999" | r.reclass --o input=buf_mask output=buf_mask_new rules=-')
        pReclass = grass_feed_command('r.reclass',
            input='buf_mask', output='buf_mask_new', rules='-',
            overwrite='True')
        pReclass.stdin.write(encode('2 = 2 9999999\n'))
        pReclass.stdin.close()
        pReclass.wait()
        
        grass_run_command('r.patch',
            input='buf_mask_new,test', output='sum_buf', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
        grass_run_command('r.mapcalc',
            expression='newmap3=if((isnull(newmap_sz1)),newmap,eval(a=@sum_buf[0,-1],b=@sum_buf[-1,0],c=@sum_buf[0,1],d=@sum_buf[1,0],m=min(a,b,c,d),if((m==int(a)),sum_buf[0,-1],if((m==int(b)),sum_buf[-1,0],if((m==int(c)),sum_buf[0,1],if((m==int(d)),sum_buf[1,0]))))))',
            overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
        
        # START ELIMINATING NOW FOR AREAS GREATER THAN 1 PIXEL
        grass_run_command('r.mask', raster='subbasins', maskcats=id.rstrip('\n'), overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
        
        for sz in iterable2:
           
            if not generator:
                iterable2.set_description('[process %s] basin [%s/%s] id %s group size %s' %
                    (processN, padLeft(str(iRun), len(str(nbBasins))), nbBasins, pad(id.strip(), 6), pad(str(sz-1), 4))
                )
           
            if sz not in getAreasUpTo('newmap3', min_area):
                pass
                # I should create a MASK here to prevent the MASK removal to send an error later in the code...
                # Or I could just remove the mask removal of before
            else:
                initmap = 'newmap3'
                counter_old = -1
                
                while (True):
                    
                    counter = count_only(initmap, sz)
                    if (counter != counter_old):
                        reclass(initmap, 'clumps_sz2', sz)
                        counter_old = counter
                        grass_run_command('r.mapcalc', expression='base=clumps_sz2', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                        grass_run_command('r.null', map='base', null=0, stdout=DEVNULL, stderr=DEVNULL)
                        grass_run_command('r.mapcalc',
                            expression='map_d=if((not(isnull(base))),eval(a=base[0,-1],b=base[-1,0],c=base[0,1],d=base[1,0],i=base,max(a,b,c,d,i)))',
                            overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                        grass_run_command('r.statistics', base='base', cover='map_d', method='mode', output='map1', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                       
                        #os.system("r.stats -lnN input=map1 | tail -n +2 | awk '{print $1 \" = \" $2}' | r.reclass --o input=base output=base_new rules=-")
                        p = grass_pipe_command('r.stats', quiet=True, flags='lnN', input='map1')
                        pReclass = grass_feed_command('r.reclass', overwrite=True, input='base', output='base_new', rules='-')
                        # skip first line "0 0"
                 
                        #    l = p.stdout.readline()



                        for l in p.stdout:
                            lSpl = decode(l).rstrip(os.linesep).split()
                            pReclass.stdin.write(encode('%s = %s\n' % (lSpl[0], lSpl[1])))
                        p.wait()
                        pReclass.stdin.close()
                        pReclass.wait()
                       
                        grass_run_command('r.patch',
                            input='base_new,newmap3', output='newout', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                        # TODO there was a mapcalc here in v4 which was removed in v5...why?
                        initmap = 'newmap3'
                   
                    else:
                        # ELIMINATION STOPPED
                        break
                # TODO check if moving this mask changes the results
                #grass_run_command('r.mask', raster='subbasins', maskcats=id.rstrip("\n"), overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

                #os.system("r.stats -cnN input=newmap3 | awk '{print $1 \" = \" $1 \" \" $2}' | r.reclass --o input=newmap3 output=newout_sz rules=-")
                p = grass_pipe_command('r.stats', flags='cnN', input='newmap3', quiet=True)
                pReclass = grass_feed_command('r.reclass', overwrite=True, input='newmap3', output='newout_sz', rules='-')
                for l in p.stdout:
                    lSpl = decode(l).rstrip(os.linesep).split()
                    pReclass.stdin.write(encode('%s = %s %s\n' % (lSpl[0], lSpl[0], lSpl[1])))
                p.wait()
                pReclass.stdin.close()
                pReclass.wait()
              
                reclass('newmap3', 'newmap_sz2', sz)
                grass_run_command('r.buffer', input='newmap_sz2', output='buffer', distances='%d'%buffer_distance,
                                    overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

                #os.system('echo "1 thru 2 = 1" | r.reclass --o input=buffer output=buffer_new rules=-')
                pReclass = grass_feed_command('r.reclass', overwrite=True, input='buffer', output='buffer_new', rules='-')
                pReclass.stdin.write(encode('1 thru 2 = 1\n'))
                pReclass.stdin.close()
                pReclass.wait()
                
                grass_run_command('r.mapcalc', expression='b1=if((buffer_new==1&&isnull(newmap_sz2)),newmap_sz2[-1,0],null())', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                grass_run_command('r.mapcalc', expression='b2=if((buffer_new==1&&isnull(newmap_sz2)),newmap_sz2[1,0],null())', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                grass_run_command('r.mapcalc', expression='b3=if((buffer_new==1&&isnull(newmap_sz2)),newmap_sz2[0,-1],null())', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                grass_run_command('r.mapcalc', expression='b4=if((buffer_new==1&&isnull(newmap_sz2)),newmap_sz2[0,1],null())', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                grass_run_command('r.patch',
                    input='b1,b2,b3,b4', output='b', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                grass_run_command('r.mapcalc', expression='buffer1=if((buffer==2),int(@newout_sz),null())', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                grass_run_command('r.statistics',
                    base='b', cover='buffer1', method='min', output='buffer_min',
                    overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                grass_run_command('r.mapcalc',
                    expression='buffer3=if((buffer1==@buffer_min),newmap3,null())',
                    overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                grass_run_command('r.statistics',
                    base='buffer_min', cover='buffer3', method='max', output='final',
                    overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

                #os.system("r.stats -lnN input=final | awk '{print $1 \" = \" $2}' | r.reclass --o input=newmap_sz2 output=newmap_sz2_rec rules=-")
                p = grass_pipe_command('r.stats', quiet=True, flags='lnN', input='final')
                pReclass = grass_feed_command('r.reclass', overwrite=True, input='newmap_sz2', output='newmap_sz2_rec', rules='-')
                for l in p.stdout:
                    lSpl = decode(l).rstrip(os.linesep).split()
                    pReclass.stdin.write(encode('%s = %s\n' % (lSpl[0], lSpl[1])))
                p.wait()
                pReclass.stdin.close()
                pReclass.wait()
                
                grass_run_command('r.patch',
                    input='newmap_sz2_rec,newmap3', output='newout2',
                    overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                
                grass_run_command('r.mapcalc', expression='newmap3=newout2', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
                
        # SOLVE STRANGE PROBLEM OF EDGED WATERSHEDS IF EXISTS
        grass_run_command('r.mapcalc',
            expression='map_a=if((not(isnull(newmap3))&&isnull(newmap3[-1,0])&&isnull(newmap3[1,0])&&'+
                        'isnull(newmap3[0,-1])&&isnull(newmap3[0,1])),-1,newmap3)',
            overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
        
        check = int(decode(grass_read_command('r.stats', quiet=True, flags='lnN', input='map_a')).rstrip(os.linesep).split(os.linesep)[0].strip())
        if (check != -1):
            # NOTHING TO DO
            pass
        else:
            
            grass_run_command('r.mapcalc',
                expression='map_b=if((map_a==-1),if((map_a[-1,1]>-1|||map_a[1,1]>-1|||map_a[1,-1]>-1|||map_a[-1,-1]>-1),-2,null()),map_a)',
                overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
            grass_run_command('r.mapcalc',
                expression='map_c=if((map_b==-2),if((not(isnull(map_b[-1,1]))),map_b[-1,1],if((not(isnull(map_b[1,1]))),map_b[1,1],if((not(isnull(map_b[1,-1]))),map_b[1,-1],if((not(isnull(map_b[-1,-1]))),map_b[-1,-1])))),map_b)',
                overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
            grass_run_command('r.mapcalc',
                expression='map_d=if((map_a==-1),1,null())',
                overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

            grass_run_command('r.buffer',
                input='map_d', output='map_d2', distances='%d'%buffer_distance,
                overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

            #os.system('echo "1 thru 2 = 1" | r.reclass --o input=map_d2 output=map_d3 rules=-')
            pReclass = grass_feed_command('r.reclass', overwrite=True, input='map_d2', output='map_d3', rules='-')
            pReclass.stdin.write(encode('1 thru 2 = 1\n'))
            pReclass.stdin.close()
            pReclass.wait()
            
            grass_run_command('g.remove', flags='f', type='raster', name='MASK', stdout=DEVNULL, stderr=DEVNULL)
            grass_run_command('r.clump',
                input='map_d3', output='map_d4',
                overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
            grass_run_command('r.mapcalc',
                expression='map_d5=if((not(isnull(map_d4[-1,0]))&&not(isnull(map_d4[1,0]))&&not(isnull(map_d4[0,-1]))&&not(isnull(map_d4[0,1]))),map_d4,null())',
                overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
            
            grass_run_command('r.statistics',
                base='map_d5', cover='map_c', method='max', output='map_f',
                overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
            grass_run_command('r.mapcalc',
                expression='map_g=int(@map_f)',
                overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
            grass_run_command('r.patch',
                input='map_g,newmap3', output='map_h',
                overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
            grass_run_command('r.mapcalc',
                expression='newmap3=map_h',
                overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
            
    grass_run_command('g.remove', flags='f', type='raster', name='MASK', stdout=DEVNULL, stderr=DEVNULL)
    
    grass_run_command('g.region', raster='clumps', stdout=DEVNULL, stderr=DEVNULL)
    
    grass_run_command('r.patch',
        input='result,newmap3', output='result_tmp',
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    
    grass_run_command('r.mapcalc',
        expression='result=result_tmp',
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    
    grass_run_command('r.mapcalc',
        expression='newmap3=null()',
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    
    grass_run_command('r.mapcalc',
        expression='clumps_cp=clumps',
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    #i += 1

'''
MAIN
'''
def main(parms_file, nbProc, generator=True):
#def main(parms_file, nbProc, generator=False):
    os.environ['GRASS_MESSAGE_FORMAT'] = 'silent'

    timestr = time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime())
    print('---------- HRU-delin Step 3 started -------------------------- %s' % str(timestr))

    configFileDir = os.path.dirname(parms_file)
    # create main env
    buildGrassEnv(os.path.join(configFileDir, 'grass_db'), 'hru-delin')
    os.environ['GISRC'] = os.path.join(configFileDir, 'grass_db', 'grassdata', 'hru-delin', '.grassrc')
    # Get parameters from configuration file
    parms = ConfigParser.ConfigParser(allow_no_value=True)
    tmpPath = os.path.join(configFileDir, 'tmp')
    if not os.path.isdir(tmpPath):
        os.mkdir(tmpPath)
    parms.read(parms_file)
    directory_out = parms.get('dir_out', 'files')
    # manage absolute and relative paths
    if not os.path.isabs(directory_out):
        directory_out = os.path.join(configFileDir, directory_out)

    # test if surface is valid
    if not isint(parms.get('hrus_min_surface', 'surface')):
        sys.exit('------------> ERROR : Hrus min surface not provided or is not integer' ) 

    ## if hgeon, landuse and soil provided, test if exist
    directory = parms.get('dir_in', 'dir')
    for data in parms.items('data'):
        data = os.path.join(directory, data[1])
        if not os.path.isfile(data):
            
            sys.exit('------------> ERROR : Input data not found' )



    min_area = int(parms.get('hrus_min_surface', 'surface'))

    # get rid of mask anyway
    grass_run_command('g.remove', flags='f', type='raster', name='MASK', stdout=DEVNULL, stderr=DEVNULL)

    # Import rasters in grass envir
    print('---------- HRU-delin Step 3 : Importing the rasters in GRASS')

    dem_recl = os.path.join(directory_out, 'step1_dem_reclass.tif')
    grass_run_command('g.proj', flags='c', georef=dem_recl, stdout=DEVNULL, stderr=DEVNULL)

    data_list = []
    mask_list = []
    if parms.get('layer_overlay', 'dem') == 'yes':
        print('----------------------------- Importing raster \'step1_dem_reclass.tif\'')
        dem = 'dem'
        grass_run_command('r.in.gdal', flags='o', input=dem_recl, output=dem, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
        data_list.append('dem')
        mask_list.append('dem_msk')

    if parms.get('layer_overlay', 'slope') == 'yes':
        print('----------------------------- Importing raster \'step1_slope_reclass.tif\'')
        slp_recl = os.path.join(directory_out, 'step1_slope_reclass.tif')
        grass_run_command('r.in.gdal', flags='o', input=slp_recl, output='slp', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
        data_list.append('slp')
        mask_list.append('slp_msk')

    if parms.get('layer_overlay', 'aspect') == 'yes':
        print('----------------------------- Importing raster \'step1_aspect_reclass.tif\'')
        asp_recl = os.path.join(directory_out, 'step1_aspect_reclass.tif')
        grass_run_command('r.in.gdal', flags='o', input=asp_recl, output='asp', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
        data_list.append('asp')
        mask_list.append('asp_msk')

   
    # read sub-basin raster created in step 1
    print('----------------------------- Importing raster \'step2_subbasins_2.tif\'')
    # new raster I. H.
    subbasins = os.path.join(directory_out, 'step2_subbasins_2.tif')
    grass_run_command('r.in.gdal', flags='o', input=subbasins, output='subbasins', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    data_list.append('subbasins')
    mask_list.append('subbasins_msk')

    # read watershed raster created in step 2
    print('----------------------------- Importing raster \'step2_watersheds.tif\'')
    watersheds = 'watersheds'
    basins = os.path.join(directory_out, 'step2_watersheds.tif')
    grass_run_command('r.in.gdal', flags='o', input=basins, output='watersheds', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    data_list.append('watersheds')
    mask_list.append('watersheds_msk')

    for data in parms.items('data'):
        print('----------------------------- Importing raster \'step1_%s\'' % str(data[1]))
        name = data[0]
        data_file = (os.path.join(directory_out, 'step1_' + data[1]))
        grass_run_command('r.in.gdal',
            flags='o', input=data_file,
            output=name, overwrite='True', stdout=DEVNULL, stderr=DEVNULL
        )
        data_list.append(name)
        mask_list.append(name + '_msk')

    grass_run_command('g.remove', flags='f', type='raster', name='MASK', stdout=DEVNULL, stderr=DEVNULL)

    grass_run_command('g.region', raster='watersheds', stdout=DEVNULL, stderr=DEVNULL)

    # Import mask(= all watersheds)
    print('----------------------------- Importing mask raster \'step2_mask\'')
    mask_in = os.path.join(directory_out, 'step2_mask.tif')
    mask_wk = 'mask_wk'
    grass_run_command('r.in.gdal', flags='o', input=mask_in, output=mask_wk, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.null', map=mask_wk, null=0, setnull=1, stdout=DEVNULL, stderr=DEVNULL)
    # Cut out every layer with mask
    for layer in data_list:
        print('----------------------------- Cutting layer \''+str(layer)+'\' according to mask')
        grass_run_command('r.patch', input='mask_wk,%s' %(layer), output='%s_msk' %(layer), overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
        grass_run_command('r.null', map='%s_msk' % layer, setnull=0, stdout=DEVNULL, stderr=DEVNULL)

    # Create a first raster of hrus = combination (overlay) of layers
    timestr = time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime())
    print('---------- HRU-delin Step 3 : Creating the cross product of the layers --- '+ str(timestr))
    print('----------------------------- Computing the raw cross product')
    # r.cross has changed a lot in grass7, combinations with at least one NULL (not all) are now absent
    # so we remove this option and reclass those combinations to get exactly what we with grass6
    # tested and approved
    grass_run_command('r.cross',
        input=','.join(mask_list),
        output='hrus_tmp_wrong', overwrite='True',
        stdout=DEVNULL, stderr=DEVNULL
    )
    # generate reclass rules
    p = grass_pipe_command('r.stats', flags='l', input='hrus_tmp_wrong')
    pReclass = grass_feed_command('r.reclass', input='hrus_tmp_wrong', output='hrus_tmp', rules='-', overwrite='True')
    c = 1
    for l in p.stdout:
        dl = decode(l).rstrip(os.linesep)
        spl = dl.split()
        cat = spl[0]
        label = ' '.join(spl[1:])
        # used to be this but does not work in grass74 because result of r.cross is slightly different than grass >= 76...
        #if 'NULL' in l:
        if not dl.startswith('*') and ('NULL' in dl or 'no data' in dl or dl.strip() == '0'):
            pReclass.stdin.write(encode('%s = 0\n' % (cat)))
        else:
            pReclass.stdin.write(encode('%s = %s %s\n' % (cat, c, label)))
            c += 1
    p.wait()
    pReclass.stdin.close()
    pReclass.wait()

    print('----------------------------- Saving the raw cross product')
    grass_run_command('r.out.gdal',
        input='hrus_tmp',
        output=os.path.join(directory_out, 'step3_hrutmp.tif'),
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    # Attrib a unique cat to each HRU generated
    print('----------------------------- Attributing unique IDs to HRUs')
    grass_run_command('r.clump', input='hrus_tmp', output='clumps', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.out.gdal',
        input='clumps',
        output=os.path.join(directory_out, 'step3_clumps.tif'),
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)


    # Start merging
    print('----------------------------- Sarting merging HRUs')
    grass_run_command('g.region', raster='clumps', stdout=DEVNULL, stderr=DEVNULL)
    #buffer_distance = int(math.ceil(float(os.popen("g.region -m").readlines()[4].split("=")[1])))
    #buffer_distance_ew = int(math.ceil(float(os.popen("g.region -m").readlines()[5].split("=")[1])))
    # g.region result parsing is now safer, it does not depend on line ordering
    regionOutput = decode(grass_read_command('g.region', flags='m')).rstrip(os.linesep).split(os.linesep)
    buffer_distance = None
    buffer_distance_ew = None
    for ro in regionOutput:
        if ro.startswith('nsres'):
            buffer_distance = int(math.ceil(float(ro.split('=')[1])))
        elif ro.startswith('ewres'):
            buffer_distance_ew = int(math.ceil(float(ro.split('=')[1])))

    if abs(buffer_distance) != abs(buffer_distance_ew):
        max_buffer = max(buffer_distance, buffer_distance_ew) + 1
        buffer_distance = max_buffer

    if buffer_distance == None or buffer_distance_ew == None:
        sys.exit('problem getting clumps resolution')

    print('----------------------------- Extracting subbasins IDs to use as units in loop')
    grass_run_command('r.mask', raster='clumps', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    basin_ids = decode(grass_read_command('r.stats', quiet=True, flags='nN', input='subbasins')).rstrip(os.linesep).split(os.linesep)
    

    grass_run_command('r.mapcalc', expression='clumps_cp=clumps', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    result = 'result'
    grass_run_command('r.mapcalc', expression='%s=null()' % result, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    timestr = time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime())
    print('---------- HRU-delin Step 3 : Starting HRUs construction, may take some time ... --- %s' % str(timestr))
    print('\n------------------------------------------------------------------------------\n')

    # Subbasin parallel loop

    # export rasters that are necessary for parallel environments
    rastersForWorkers = {
        'subbasins_msk': os.path.join(tmpPath, 'step3_subbasins_msk.tif'),
        'clumps': os.path.join(tmpPath, 'step3_clumps.tif'),
        'clumps_cp': os.path.join(tmpPath, 'step3_clumps_cp.tif'),
        'subbasins': os.path.join(tmpPath, 'step3_subbasins.tif')
    }
    exportRasters(rastersForWorkers)

    # save main grass env which is being overriden later
    MAIN_GISRC = os.environ['GISRC']

    # build the environments and load exported rasters in each of them
    grassDbPath = os.path.join(configFileDir, 'grass_db')
    for i in range(nbProc):
        location = 'hru-delin_%s' % (i+1)
        buildGrassLocation(grassDbPath, location)
        importRastersInEnv(rastersForWorkers, grassDbPath, location)
        # init result raster to null in all environments
        os.environ['GISRC'] = os.path.join(grassDbPath, 'grassdata', location, '.grassrc')
        grass_run_command('r.mapcalc', expression='result=null()', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    elems = basin_ids
    nbElems = len(elems)
    # the locks are here to prevent concurrent terminal tqdm writing
    # this is the interesting part, launching N processes in parallel to process basins
    
    if generator:
        print('Starting subbasins loop with %s process' % nbProc)
        
        with Pool(nbProc) as p:
            
            params = [(id, configFileDir, i+1, nbElems, min_area, buffer_distance, tmpPath, generator, nbProc) for (i, id) in enumerate(elems)]
           
            for i, _ in enumerate(p.imap_unordered(processSubbasin, params), 1):
           
                yield (i/nbElems*100)
                
           
    else:
        
        with Pool(nbProc, initializer=tqdm.set_lock, initargs=(tqdm.get_lock(),)) as p:
             
            params = [(id, configFileDir, i+1, nbElems, min_area, buffer_distance, tmpPath, generator, nbProc) for (i, id) in enumerate(elems)]
           
            r = list(tqdm(p.imap_unordered(processSubbasin, params),
                desc='[main process] Loop on subbasins [%s process] ' % nbProc,
                total=nbElems,
                unit='basin',
                bar_format=bar_format1
            ))
            
       
    # export rasters from parallel grass environments to files
    for i in range(nbProc):
        location = 'hru-delin_%s' % (i+1)
        
        rastersToExport = {
            'result': os.path.join(tmpPath, 'step3_result_%s.tif' % (i+1))
        }
        exportRastersFromEnv(rastersToExport, grassDbPath, location)
     
    # restore main grass env
    os.environ['GISRC'] = MAIN_GISRC

    # merge results from files to result raster
    for i in range(nbProc):
        n = i + 1

        one_result_path = os.path.join(tmpPath, 'step3_result_%s.tif' % n)
        grass_run_command('r.in.gdal', flags='o', input=one_result_path, output='one_result', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

        grass_run_command('r.patch',
            input='result,one_result', output='result_tmp',
            overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
        grass_run_command('r.mapcalc',
            expression='result=result_tmp',
            overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
       
    print('')
    timestr = time.strftime("%a, %d %b %Y, %H:%M:%S", time.localtime())
    print('---------- HRU-delin Step 3 : Reassign category values of HRUs --- '+str(timestr))

    #os.system("r.stats -nN input=result | awk 'BEGIN{i=1} {print $1 \"=\" i;i++} END{}' > %s/result_rules" % tmpPath)
    #grass_run_command('r.reclass',
    #    input='result', output='result_rcl', rules=os.path.join(tmpPath, 'result_rules'),
    #    overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    p = grass_pipe_command('r.stats', quiet=True, flags='nN', input='result')
    pReclass = grass_feed_command('r.reclass', overwrite=True, input='result', output='result_rcl', rules='-')
    i = 1
    for l in p.stdout:
        lSpl = decode(l).rstrip(os.linesep).split()
        pReclass.stdin.write(encode('%s = %s\n' % (lSpl[0], i)))
        i += 1
    p.wait()
    pReclass.stdin.close()
    pReclass.wait()

    grass_run_command('r.mapcalc',
        expression='result_rcl2=if((result_rcl==0),null(),result_rcl)',
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    grass_run_command('r.colors', map='result_rcl2', color='random', stdout=DEVNULL, stderr=DEVNULL)
    hrus = 'result_rcl2'

    # added by IH: I don't know why I added the following but I think it is the cause of the issue I'm have step 4 (empty topology...)
    # And from what I understand it doesn't make much sense here.
    # grass_run_command('db.droptable', flags='f', table='hrus_v_cat')
    # grass_run_command('r.to.vect', flags='v', input=hrus, output='hrus_v_cat', feature='area', overwrite='True')

    # Save rasters
    grass_run_command('r.out.gdal',
        input=hrus, output=os.path.join(directory_out, 'step3_hrus.tif'),
        overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    timestr = time.strftime('%a, %d %b %Y, %H:%M:%S', time.localtime())
    print('---------- HRU-delin Step 3 ended   -------------------------- '+str(timestr))

if __name__ == '__main__':
    from grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv, exportRastersFromEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_pipe_command, grass_read_command
    from progressColors import *

    try:
        from tqdm import tqdm
    except Exception as e:
        print('!! %stqdm module not found. %s\n' % (COLOR_RED, COLOR_RESET))
        sys.exit(1)

    parms_file = 'hrudelin_config.cfg'
    nbProcArg = ''
    if len(sys.argv) > 1:
        parms_file = sys.argv[1]
        if len(sys.argv) > 2:
            nbProcArg = sys.argv[2]
    # determine how many processes we can launch
    if str(nbProcArg).isnumeric() and int(nbProcArg) > 0:
        nbProc = int(nbProcArg)
    else:
        nbProc = cpu_count()

    # just because it's a generator (not used here but in the Qgis plugin)
    res = list(main(parms_file, nbProc, False))

    try:
        os.system('notify-send "hru-delin-6-2 step 3 complete"')
    except Exception as e:
        pass
else:
    from .grassUtils import buildGrassEnv, buildGrassLocation, exportRasters, importRastersInEnv, exportRastersFromEnv,\
        grass_run_command, grass_parse_command, grass_feed_command, grass_pipe_command, grass_read_command
    from .progressColors import *
