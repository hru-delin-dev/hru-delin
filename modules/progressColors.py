#!/usr/bin/env python3
# -*- coding: utf-8 -*-

############################################################################
#
# MODULE:       progressColors.py
# AUTHOR(S):    Julien Veyssier
#               
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################






COLOR_GREEN='\x1b[32m'
COLOR_YELLOW='\x1b[33m'
COLOR_CYAN='\x1b[36m'
COLOR_BLUE='\x1b[34m'
COLOR_WHITE='\x1b[37m'
COLOR_RED='\x1b[31m'
COLOR_RESET='\x1b[39m'
BACK_BLACK='\x1b[40m'
STYLE_BRIGHT='\x1b[1m'
STYLE_RESET='\x1b[0m'

# left and right parts of progress line
l_bar1 = '{desc}%s%s{percentage:3.0f}%%%s|' % (STYLE_BRIGHT, COLOR_GREEN, STYLE_RESET)
l_bar_yellow = '{desc}%s%s{percentage:3.0f}%%%s|' % (STYLE_BRIGHT, COLOR_YELLOW, STYLE_RESET)
l_bar2 = '{desc}{percentage:3.0f}%|'
r_bar1 = '| %s%s{n_fmt}/{total_fmt}%s [{elapsed}<{remaining}, {rate_fmt}{postfix}]' % (STYLE_BRIGHT, COLOR_GREEN, STYLE_RESET)
r_bar_yellow = '| %s%s{n_fmt}/{total_fmt}%s [{elapsed}<{remaining}, {rate_fmt}{postfix}]' % (STYLE_BRIGHT, COLOR_YELLOW, STYLE_RESET)

# default bar format
bar_format0='{l_bar}{bar}{r_bar}'
bar_format1='%s%s{bar}%s%s' % (l_bar1, COLOR_GREEN, STYLE_RESET, r_bar1)
bar_format_yellow_2='%s%s{bar}%s%s' % (l_bar_yellow, COLOR_YELLOW, STYLE_RESET, r_bar_yellow)
bar_format2='%s{bar}{r_bar}' % (l_bar2)

def pad(s, nb):
    while len(s) < nb:
        s += ' '
    return s

def padLeft(s, nb):
    while len(s) < nb:
        s = ' ' + s
    return s
