#!/usr/bin/env python3
# -*- coding: utf-8 -*-

############################################################################
#
# MODULE:       utils.py
# AUTHOR(S):    Julien Veyssier
#               
#
# COPYRIGHT:    (C) 2020 UR RIVERLY - INRAE
#
#               This program is free software under the GNU General Public
#               License (>=v2). Read the file LICENSE that comes with 
#                HRU-DELIN for details.
#
#############################################################################



import os, sys
import pandas as pd
from osgeo import gdal
from osgeo import ogr
from osgeo import osr

def myJoin(f1Path, f2Path, resultPath):
    df1 = pd.read_table(f1Path, delim_whitespace=True)
    df2 = pd.read_table(f2Path, delim_whitespace=True)
    merge = pd.merge(df1, df2)
    merge.to_csv(resultPath, header=True, index=False, sep=' ')

def cutting_raster(raster,raster_out, xmin_slect, ymax_slect, xmax_slect, ymin_slect):
    ds = gdal.Open(raster)
    ds = gdal.Translate(raster_out, ds, projWin=[xmin_slect, ymax_slect, xmax_slect, ymin_slect])
    ds = None

def get_raster_bounds(layer, xmin_slect, ymax_slect, xmax_slect, ymin_slect):
    # this is better than sketchy "gdal.Info(ds).split('\n')"
    src = gdal.Open(layer)
    xmin, xres, xskew, ymax, yskew, yres = src.GetGeoTransform()
    xmax = xmin + (src.RasterXSize * xres)
    ymin = ymax + (src.RasterYSize * yres)
    src = None
    return (
        max(xmin_slect, xmin),
        min(ymax_slect, ymax),
        min(xmax_slect, xmax),
        max(ymin_slect, ymin)
    )

def get_polygon_bounds(shape, xmin_slect, ymax_slect, xmax_slect, ymin_slect):
    ds = ogr.Open(shape)
    layer = ds.GetLayer()
    xmin, xmax, ymin, ymax = layer.GetExtent()
    return (
        max(xmin_slect, xmin),
        min(ymax_slect, ymax),
        min(xmax_slect, xmax),
        max(ymin_slect, ymin)
    )



def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

def isint(value):
    try:
        int(value)
        return True
    except ValueError:
        return False

# cutting the rasters according to user selection
def get_coords(parms, xmin_slect, ymax_slect, xmax_slect, ymin_slect):

    if not isfloat(parms.get('surface', 'west')) or isfloat(parms.get('surface', 'north')) or isfloat(parms.get('surface', 'east')) or isfloat(parms.get('surface', 'south')):
        sys.exit('------------> ERROR : coordinates for surface selection is not valid')

    xmin_slect = max(xmin_slect, float(parms.get('surface', 'west')))
    ymax_slect = min(ymax_slect, float(parms.get('surface', 'north')))
    xmax_slect = min(xmax_slect, float(parms.get('surface', 'east')))
    ymin_slect = max(ymin_slect, float(parms.get('surface', 'south')))
    print("hello",xmin_slect)
    return xmin_slect, ymax_slect, xmax_slect, ymin_slect



def is_valid_shp(shplayer):
    
    if not os.path.isfile(shplayer):
        msg="------------> ERROR : Input "+ shplayer+" not found"
        sys.exit(msg)
    if ogr.Open(shplayer) is None:
        msg="------------> ERROR : Input "+ shplayer+" is not a valid shapefile"
        sys.exit(msg) 


def is_valid_geometry(Name,GeometryType):
    NameDs=ogr.Open(Name)
    NameLayer=NameDs.GetLayer()
    NameLayer_defn=NameLayer.GetLayerDefn()
    if ogr.GeometryTypeToName(NameLayer_defn.GetGeomType()) != GeometryType:
        msg="------------> ERROR : Input  "+ Name+" is not "+ GeometryType
        sys.exit(msg)


def is_column_exist(Name,colname):
    NameDs=ogr.Open(Name)
    NameLayer=NameDs.GetLayer()
    NameLayer_defn=NameLayer.GetLayerDefn()
    FieldFound=False
    for n in range(NameLayer_defn.GetFieldCount()):
        fdefn=NameLayer_defn.GetFieldDefn(n)
        if fdefn.name == colname:
            FieldFound=True

    if not FieldFound:
        msg="------------> ERROR : column "+ colname+ " not found in Input  "+Name
        sys.exit(msg)

def is_column_valid(Name,colname):    
    NameDs=ogr.Open(Name)
    NameLayer=NameDs.GetLayer()
    listvalue= [ feature.GetField(colname) for feature in NameLayer ]
    
    if None in listvalue or (min(listvalue) == 0):
        msg="ERROR : in column "+ colname+ " in Input "+Name +" null or zero value detected"
        sys.exit(msg)

def is_column_value_unique(Name,colname):
    NameDs=ogr.Open(Name)
    NameLayer=NameDs.GetLayer()
    listvalue= [ feature.GetField(colname) for feature in NameLayer ]

    if(len(set(listvalue)) != len(listvalue)):
        msg="ERROR : in column "+ colname+ " in Input "+Name +" non unique value detected"
        sys.exit(msg)

def are_columns_value_unique(Name1,colname1,Name2,colname2):
    NameDs1=ogr.Open(Name1)
    NameLayer1=NameDs1.GetLayer()
    listvalue1= [ feature.GetField(colname1) for feature in NameLayer1 ]

    NameDs2=ogr.Open(Name2)
    NameLayer2=NameDs2.GetLayer()
    listvalue2= [ feature.GetField(colname2) for feature in NameLayer2 ]
    listvalue1.extend(listvalue2)


    if(len(set(listvalue1)) != len(listvalue1)):
        msg="ERROR : in column "+ colname1+ " in Input "+Name1 +" and in column "+colname2+" in Input "+Name2+" duplicate value !"
        sys.exit(msg)

def write_log(logf,msg,  start, end, best_x, best_y,point_code,point_type,point_area):
    horiz_shift = best_x - start - end
    vertic_shift = best_y - start - end + 1
    if horiz_shift != 0 or vertic_shift != 0:
        if horiz_shift > 0:
            dir_x_shift = 'East'
        else:
            dir_x_shift = 'West'

        if vertic_shift > 0:
            dir_y_shift = 'South'
        else:
            dir_y_shift = 'North'

        logf.writerow({
            'point code': point_code,
            'point type': point_type,
            'point area': str(point_area),
            'rule': msg,
            'horiz dir': dir_x_shift,
            'horiz shift': str(abs(horiz_shift)),
            'vertic dir': dir_y_shift,
            'vertic shift': str(abs(vertic_shift))
        })

def get_Raster_EPSG(rasterName):
    RasterFile=gdal.Open(rasterName)
    crs=osr.SpatialReference(wkt=RasterFile.GetProjection())
    if crs:
        return crs.GetAttrValue('AUTHORITY',1)
    else:
        return 0    
    

def get_Vector_EPSG(vectorName):
    NameDs=ogr.Open(vectorName)
    NameLayer=NameDs.GetLayer()
    crs=NameLayer.GetSpatialRef()
    if crs:
        return crs.GetAttrValue('AUTHORITY',1)
    else:
        return 0    

