#!/bin/bash


MYPATH=`readlink -f $0`
MYDIR=`dirname $MYPATH`


NBSEP=$(echo "$MYDIR"|grep -o '/'|wc -l)
DIRPATH=$(echo "$MYDIR"|cut -d/ -f1-$NBSEP)


DIRTEST="${MYDIR}/tmptest"
DIRDATATEST="${MYDIR}/datatests"

mkdir $DIRTEST

`echo "ls"`|grep ".cfg" >$DIRTEST/listcfg

for i in `cat $DIRTEST/listcfg`;do
     sed -e "s|__DIR_HRU_DELIN__|$DIRDATATEST|g" $i > $DIRTEST/$i
  
done


succeed=0
failed=0
count=0



echo "**** test hru-delin_step1.sh ****"
# test input directory
let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/bad_input_dir_config.cfg >out 2>/dev/null

if [ $? -ne 0 ];then
    echo "test input directory ----- ok"
    let succeed++
else
    echo "test input directory ------------ FAILED"
    let failed++
fi



## test input dem
let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/no_dem_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if dem exist ----- ok"
    let succeed++
else
    echo "test if dem exist ------------ FAILED"
    let failed++
fi

let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/bad_dem_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if dem is valid ----- ok"
    let succeed++
else
    echo "test if dem is valid ------------ FAILED"
    let failed++
fi


## test gauges
let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/no_gauges_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if Gauges exist ----- ok"
    let succeed++
else
    echo "test if Gauges exist ------------ FAILED"
    let failed++
fi

let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/bad_gauges_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if Gauges is valid ----- ok"
    let succeed++
else
    echo "test if Gauges is valid ------------ FAILED"
    let failed++
fi

let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/multipoint_gauges_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if Gauges is point geometry ----- ok"
    let succeed++
else
    echo "test if Gauges is point geometry ------------ FAILED"
    let failed++
fi


let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/null_value_gauge_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if Gauges has null value in col_name ----- ok"
    let succeed++
else
    echo "test if Gauges has null value in col_name ------------ FAILED"
    let failed++
fi




## test data is valid
let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/bad_data_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if data exist ----- ok"
    let succeed++
else
    echo "test if data exist ------------ FAILED"
    let failed++
fi

## test output FILE and RESULTS Directory
let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/empty_output_files_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Directory is provided ----- ok"
    let succeed++
else
    echo "test if output FILE Directory is provided ------------ FAILED"
    let failed++
fi


let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/empty_output_results_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Results is provided ----- ok"
    let succeed++
else
    echo "test if output FILE Results is provided ------------ FAILED"
    let failed++
fi

## test output FILE and RESULTS Directory is valid
let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/bad_output_files_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Directory is valid ----- ok"
    let succeed++
else
    echo "test if output FILE Directory is valid ------------ FAILED"
    let failed++
fi


let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/bad_output_results_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Results is valid ----- ok"
    let succeed++
else
    echo "test if output FILE Results is valid ------------ FAILED"
    let failed++
fi




## test irrigation raster if provided
let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/bad_irrigation_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if Irrigation is provided ----- ok"
    let succeed++
else
    echo "test if Irrigation is provided ------------ FAILED"
    let failed++
fi

## test polygon layer for surface selection if provided
let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/bad_polygon_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if Polygon layer for surface selection is valid ----- ok"
    let succeed++
else
    echo "test if Polygon layer for surface selection is valid ------------ FAILED"
    let failed++
fi

let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/no_polygon_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if Polygon layer for surface selection is provided ----- ok"
    let succeed++
else
    echo "test if Polygon layer for surface selection is provided ------------ FAILED"
    let failed++
fi


## test if coords for surface selection is valid
let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/bad_coords_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if coords for surface selection is valid ----- ok"
    let succeed++
else
    echo "test if coords for surface selection is valid ------------ FAILED"
    let failed++
fi

## test if reclass dem rules is missing
let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/bad_reclass_dem_rules_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if reclass dem rules is missing is valid ----- ok"
    let succeed++
else
    echo "test if reclass dem rules is missing is valid ------------ FAILED"
    let failed++
fi


## test if step dem is valid
let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/bad_step_dem_rules_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if step dem value is valid ----- ok"
    let succeed++
else
    echo "test if if step dem value is valid ------------ FAILED"
    let failed++
fi

## test if reclass slope rules is missing
let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/bad_reclass_slope_rules_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if reclass slope rules is missing is valid ----- ok"
    let succeed++
else
    echo "test if reclass slope rules is missing is valid ------------ FAILED"
    let failed++
fi


## test if reclass aspect rules is missing
let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/bad_reclass_aspect_rules_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if reclass aspect rules is missing is valid ----- ok"
    let succeed++
else
    echo "test if reclass aspect rules is missing is valid ------------ FAILED"
    let failed++
fi

## test if basin size value is missing
let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/bad_basin_size_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if basin size value is missing ----- ok"
    let succeed++
else
    echo "test if basin size value is missing ------------ FAILED"
    let failed++
fi

## test step1 all OK
let count++
$DIRPATH/bin/hru-delin_step1.sh  $DIRTEST/step1_ok_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if step 1 all OK  ----- FAILED"
    let failed++
else
    echo "test if step 1 all OK ------------ ok"
    let succeed++
fi
echo " " 
echo "**** test hru-delin_step2.sh ****"


# test output FILE and RESULTS Directory
let count++
$DIRPATH/bin/hru-delin_step2.sh  $DIRTEST/empty_output_files_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Directory is provided ----- ok"
    let succeed++
else
    echo "test if output FILE Directory is provided ------------ FAILED"
    let failed++
fi


let count++
$DIRPATH/bin/hru-delin_step2.sh  $DIRTEST/empty_output_results_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Results is provided ----- ok"
    let succeed++
else
    echo "test if output FILE Results is provided ------------ FAILED"
    let failed++
fi

## test output FILE and RESULTS Directory is valid
let count++
$DIRPATH/bin/hru-delin_step2.sh  $DIRTEST/bad_output_files_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Directory is valid ----- ok"
    let succeed++
else
    echo "test if output FILE Directory is valid ------------ FAILED"
    let failed++
fi


let count++
$DIRPATH/bin/hru-delin_step2.sh  $DIRTEST/bad_output_results_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Results is valid ----- ok"
    let succeed++
else
    echo "test if output FILE Results is valid ------------ FAILED"
    let failed++
fi






## test if basin size value is missing
let count++
$DIRPATH/bin/hru-delin_step2.sh  $DIRTEST/bad_basin_size_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if basin size value is missing ----- ok"
    let succeed++
else
    echo "test if basin size value is missing ------------ FAILED"
    let failed++
fi

## test if COLNAME is missing
let count++
$DIRPATH/bin/hru-delin_step2.sh  $DIRTEST/no_col_name_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if COLNAME is missing ----- ok"
    let succeed++
else
    echo "test if COLNAME is missing ------------ FAILED"
    let failed++
fi

## test if COLNAME is valid

let count++
$DIRPATH/bin/hru-delin_step2.sh  $DIRTEST/bad_col_name_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if COLNAME is valid ----- ok"
    let succeed++
else
    echo "test if COLNAME is valid ------------ FAILED"
    let failed++
fi


## test if Relocated Gauges is valid
let count++
$DIRPATH/bin/hru-delin_step2.sh  $DIRTEST/bad_relocated_gauges_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if Relocated Gauges is valid ----- ok"
    let succeed++
else
    echo "test if Relocated Gauges is valid ------------ FAILED"
    let failed++
fi

## test if Surface_tolerance_1 is valid
let count++
$DIRPATH/bin/hru-delin_step2.sh  $DIRTEST/bad_surface_tolerance_1_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if Surface_tolerance_1 is valid ----- ok"
    let succeed++
else
    echo "test if Surface_tolerance_1 is valid ------------ FAILED"
    let failed++
fi

## test if Distance_tolerance_1 is valid
let count++
$DIRPATH/bin/hru-delin_step2.sh  $DIRTEST/bad_distance_tolerance_1_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if Distance_tolerance_1 is valid ----- ok"
    let succeed++
else
    echo "test if Distance_tolerance_1 is valid ------------ FAILED"
    let failed++
fi

## test if Surface_tolerance_2 is valid
let count++
$DIRPATH/bin/hru-delin_step2.sh  $DIRTEST/bad_surface_tolerance_2_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if Surface_tolerance_2 is valid ----- ok"
    let succeed++
else
    echo "test if Surface_tolerance_2 is valid ------------ FAILED"
    let failed++
fi

## test if Distance_tolerance_2 is valid
let count++
$DIRPATH/bin/hru-delin_step2.sh  $DIRTEST/bad_distance_tolerance_2_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if Distance_tolerance_2 is valid ----- ok"
    let succeed++
else
    echo "test if Distance_tolerance_2 is valid ------------ FAILED"
    let failed++
fi

## test if Gauge area col name is provided
let count++
$DIRPATH/bin/hru-delin_step2.sh  $DIRTEST/bad_gauge_area_col_name_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if Gauge area col name is provided ----- ok"
    let succeed++
else
    echo "test if Gauge area col name is provided ------------ FAILED"
    let failed++
fi

## test if gauge area col unit is provided
let count++
$DIRPATH/bin/hru-delin_step2.sh  $DIRTEST/bad_gauge_area_col_unit_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if gauge area col unit is provided ----- ok"
    let succeed++
else
    echo "test if gauge area col unit is provided ------------ FAILED"
    let failed++
fi

## test step2 all OK
let count++

$DIRPATH/bin/hru-delin_step2.sh  $DIRTEST/step2_ok_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if step 2 all OK  ----- FAILED"
    let failed++
else
    echo "test if step 2 all OK ------------ ok"
    let succeed++
fi

echo " " 
echo "**** test hru-delin_step3.sh ****"


# test output FILE and RESULTS Directory
let count++
$DIRPATH/bin/hru-delin_step3.sh  $DIRTEST/empty_output_files_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Directory is provided ----- ok"
    let succeed++
else
    echo "test if output FILE Directory is provided ------------ FAILED"
    let failed++
fi


let count++
$DIRPATH/bin/hru-delin_step3.sh  $DIRTEST/empty_output_results_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Results is provided ----- ok"
    let succeed++
else
    echo "test if output FILE Results is provided ------------ FAILED"
    let failed++
fi

## test output FILE and RESULTS Directory is valid
let count++
$DIRPATH/bin/hru-delin_step3.sh  $DIRTEST/bad_output_files_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Directory is valid ----- ok"
    let succeed++
else
    echo "test if output FILE Directory is valid ------------ FAILED"
    let failed++
fi


let count++
$DIRPATH/bin/hru-delin_step3.sh  $DIRTEST/bad_output_results_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Results is valid ----- ok"
    let succeed++
else
    echo "test if output FILE Results is valid ------------ FAILED"
    let failed++
fi


## test if hrus min surface is valid
let count++
$DIRPATH/bin/hru-delin_step3.sh  $DIRTEST/bad_hrus_min_surface_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if hrus min surface is valid ----- ok"
    let succeed++
else
    echo "test if hrus min surface is valid ------------ FAILED"
    let failed++
fi

## test data is valid
let count++
$DIRPATH/bin/hru-delin_step3.sh  $DIRTEST/bad_data_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if data exist ----- ok"
    let succeed++
else
    echo "test if data exist ------------ FAILED"
    let failed++
fi


# test step3 all OK
let count++

$DIRPATH/bin/hru-delin_step3.sh  $DIRTEST/step3_ok_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if step 3 all OK  ----- FAILED"
    let failed++
else
    echo "test if step 3 all OK ------------ ok"
    let succeed++
fi

echo " " 
echo "**** test hru-delin_step4.sh ****"



# test output FILE and RESULTS Directory
let count++
$DIRPATH/bin/hru-delin_step4.sh  $DIRTEST/empty_output_files_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Directory is provided ----- ok"
    let succeed++
else
    echo "test if output FILE Directory is provided ------------ FAILED"
    let failed++
fi


let count++
$DIRPATH/bin/hru-delin_step4.sh  $DIRTEST/empty_output_results_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Results is provided ----- ok"
    let succeed++
else
    echo "test if output FILE Results is provided ------------ FAILED"
    let failed++
fi

## test output FILE and RESULTS Directory is valid
let count++
$DIRPATH/bin/hru-delin_step4.sh  $DIRTEST/bad_output_files_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Directory is valid ----- ok"
    let succeed++
else
    echo "test if output FILE Directory is valid ------------ FAILED"
    let failed++
fi


let count++
$DIRPATH/bin/hru-delin_step4.sh  $DIRTEST/bad_output_results_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if output FILE Results is valid ----- ok"
    let succeed++
else
    echo "test if output FILE Results is valid ------------ FAILED"
    let failed++
fi

## test data is valid
let count++
$DIRPATH/bin/hru-delin_step4.sh  $DIRTEST/bad_data_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if data exist ----- ok"
    let succeed++
else
    echo "test if data exist ------------ FAILED"
    let failed++
fi


## test irrigation raster if provided
let count++
$DIRPATH/bin/hru-delin_step4.sh  $DIRTEST/bad_irrigation_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if Irrigation is provided ----- ok"
    let succeed++
else
    echo "test if Irrigation is provided ------------ FAILED"
    let failed++
fi


# test step4 all OK
let count++

$DIRPATH/bin/hru-delin_step4.sh  $DIRTEST/step4_ok_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if step 4 all OK  ----- FAILED"
    let failed++
else
    echo "test if step 4 all OK ------------ ok"
    let succeed++
fi

echo " " 
echo "**** test all step ****"



# test all step all OK
let count++

$DIRPATH/bin/hru-delin_all-steps.sh  $DIRTEST/step4_ok_config.cfg >out 2>/dev/null 
if [ $? -ne 0 ];then
    echo "test if all step all OK  ----- FAILED"
    let failed++
else
    echo "test if all step all OK ------------ ok"
    let succeed++
fi


echo " " 
echo "    $succeed tests on $count tests succeed"
echo "    $failed tests on $count tests failed"
rm -rf ./out
rm -rf ./tmp
rm -rf $DIRTEST

